typedef struct _AbstractSource AbstractSource;
typedef void (*ReadDefsCallback)(AbstractSource *);
typedef void (*ReadTimedDataCallback)(AbstractSource *);

#define ABSTRACT_SOURCE_MEMBERS \
	CallbackHolder callbackHolder; \
	ReadDefsCallback readDefsCallback; \
	ReadTimedDataCallback readTimedDataCallback;

struct _AbstractSource{
	ABSTRACT_SOURCE_MEMBERS
};

void failOnFileMissing(const char *filePath);
