#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#define __USE_XOPEN
#include <time.h>
#include <csv.h>
#include "Pipelines.h"
#include "CallbackHolder.h"
#include "Pipelines.internal.h"
#include "AbstractSource.internal.h"
#include "list.h"
#include "utils.h"

static time_t my_timegm(struct tm *tm)
{
	time_t ret;
	char *tz;

	tz = getenv("TZ");
	if (tz) {
		tz = strdup(tz);
	}
	setenv("TZ", "", 1);
	tzset();
	ret = mktime(tm);
	if (tz) {
		setenv("TZ", tz, 1);
		free(tz);
	} else {
		unsetenv("TZ");
	}
	tzset();
	return ret;
}

uint64_t parseIsoTime(const char *timeString){
	uint64_t newTime;
	int microseconds;
	struct tm someTime;
	memset(&someTime, 0, sizeof(struct tm));
	char *newPointer = strptime(timeString, "%Y-%m-%dT%H:%M:%S", &someTime);
	if(NULL == newPointer){
		/* error */
		return 0;
	}
	newTime = my_timegm(&someTime);
	newTime *= 1000000;
	if(*newPointer == '.'){
		if(EOF != sscanf(newPointer+1, "%6d", &microseconds)){
			newTime += microseconds;
		}
	}
	return newTime;
}

typedef struct _CSVMetricSource CSVMetricSource;
struct _CSVMetricSource{
	ABSTRACT_SOURCE_MEMBERS
	double resolutionFactor; // the factor to bring timestamps to a resolution of 1000000 ticks per second
	uint64_t referencetime;
	unsigned currentField;
	unsigned currentRow;
	unsigned currentFieldIndex;
	uint64_t currentTimestamp;
	double currentValue;
	uint64_t timestampFieldIndex;
	char **valueNames;
	uint64_t *dataFieldIndexes;
	unsigned dataFieldIndexCount;
	char **unitNames;
	const char *path;
	char propertyseparator;
	char itemseparator;
	bool usesIsoTime;
	map locationEvents; /* location -> list(event) */
};

typedef struct _MetricEvent MetricEvent;
struct _MetricEvent{
	uint64_t timestamp;
	double value;
};

static void del_location_events(void *element){
	free(element);
}

static void del_location_events_list(void *from, void *to){
	list *theList = (list *)to;
	list_delete(theList, del_location_events);
	free(theList);
}

void releaseCSVMetricSource(CSVMetricSource *source){
	map_delete(&(source->locationEvents), del_location_events_list);
	free(source);
}

void afterField_cb (void *s, size_t len, void *data) {
	CSVMetricSource *source = (CSVMetricSource *)data;
	if(source->currentRow == 0){
		//read caption
		if((source->currentFieldIndex < source->dataFieldIndexCount) && (source->currentField == source->dataFieldIndexes[source->currentFieldIndex])){
			//read value
			char *name = strdup((char *)s);
			source->valueNames[source->currentFieldIndex] = name;
			//LOCATION
			OTF2_StringRef emptyWord = 0;
			OTF2_LocationRef location = source->currentFieldIndex;
			OTF2_LocationGroupRef locationGroup = 0;
			uint64_t numberOfEvents = 0;
			source->callbackHolder.onGlobalDef_Location((void *)&(source->callbackHolder), location, emptyWord, OTF2_LOCATION_TYPE_METRIC, numberOfEvents, locationGroup );
			//METRIC_MEMBER
			OTF2_StringRef unitRef = 2 * source->currentFieldIndex + 1;
			source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), unitRef, source->unitNames[source->currentFieldIndex]);
			OTF2_MetricMemberRef metricMember = source->currentFieldIndex;
			int64_t exponent = 0;
			OTF2_StringRef nameRef = 2 * source->currentFieldIndex + 2;
			OTF2_StringRef description = emptyWord;
			source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), nameRef, name);
			source->callbackHolder.onGlobalDef_MetricMember((void *)&(source->callbackHolder), metricMember, nameRef, description, OTF2_METRIC_TYPE_OTHER, OTF2_METRIC_ABSOLUTE_POINT, OTF2_TYPE_DOUBLE, OTF2_BASE_DECIMAL, exponent, unitRef);
			//METRIC_CLASS
			OTF2_MetricRef metricClass = 2 * source->currentFieldIndex;
			source->callbackHolder.onGlobalDef_MetricClass((void *)&(source->callbackHolder), metricClass, 1, &metricMember, OTF2_METRIC_ASYNCHRONOUS, OTF2_RECORDER_KIND_ABSTRACT);
			//METRIC_INSTANCE
			OTF2_MetricRef metricInstance = 2 * source->currentFieldIndex + 1;
			uint64_t scope = 1;
			source->callbackHolder.onGlobalDef_MetricInstance((void *)&(source->callbackHolder), metricInstance, metricClass, location, OTF2_SCOPE_SYSTEM_TREE_NODE, scope);
			source->currentFieldIndex++;
			list *eventList = (list *)malloc(sizeof(list));
			list_initialize(eventList);
			map_add(&(source->locationEvents), AS_VOID_PTR(location), eventList);
		}
	}else{
		if(source->currentField == source->timestampFieldIndex){
			//read timestamp
			uint64_t ts;
			if(source->usesIsoTime){
				ts = parseIsoTime(s);
				source->currentTimestamp = ts;
			}else{
				sscanf(s, "%"SCNu64, &ts);
				source->currentTimestamp = ts*source->resolutionFactor + source->referencetime;
			}
		}else if((source->currentFieldIndex < source->dataFieldIndexCount) && (source->currentField == source->dataFieldIndexes[source->currentFieldIndex])){
			//read value
			double val;
			sscanf(s, "%lf", &val);
			source->currentValue = val;

			OTF2_LocationRef location = source->currentFieldIndex;
			MetricEvent *event = (MetricEvent *)malloc(sizeof(MetricEvent));
			event->timestamp = source->currentTimestamp;
			event->value = val;
			mapentry *entry = map_find(&(source->locationEvents),AS_VOID_PTR(location));
			list *eventList = (list *)(entry->to);
			list_add(eventList, event);
			source->currentFieldIndex++;
		}
	}
	source->currentField++;
}

void afterItem_cb (int c, void *data) {
	CSVMetricSource *source = (CSVMetricSource *)data;
	if(source->currentRow == 0){
		bool wrongIndexFound = false;
		unsigned columnCount = source->currentField;
		if(columnCount <= source->timestampFieldIndex){
			fprintf(stderr, "Have read only %u columns in %s, but timestamp column index %"PRIu64" was specified. Check column indices!\n", columnCount, source->path, source->timestampFieldIndex);
			wrongIndexFound = true;
		}
		for(unsigned i = 0; i < source->dataFieldIndexCount; i++){
			uint64_t someIndex = source->dataFieldIndexes[i];
			if(columnCount <= someIndex){
				fprintf(stderr, "Have read only %u columns in %s, but column index %"PRIu64" was specified. Check column indices!\n", columnCount, source->path, someIndex);
				wrongIndexFound = true;
			}
		}
		if(wrongIndexFound){
			exit(EXIT_FAILURE);
		}
	}
	source->currentField = 0;
	source->currentFieldIndex = 0;
	source->currentRow++;
}

typedef struct _clockData clockData_t;
struct _clockData{
	uint64_t minTimestamp;
	uint64_t maxTimestamp;
};

static void iterate_locationEvents_for_clockdata(void *from, void *to, void *additionalData){
	clockData_t *clockData = (clockData_t *) additionalData;
	list *theList = (list *)to;
	if(theList->entryCount > 0){
		MetricEvent *event = (MetricEvent *) theList->entries[0];
		MetricEvent *lastEvent = (MetricEvent *) theList->entries[theList->entryCount - 1];
		clockData->minTimestamp = MIN_VALUE(clockData->minTimestamp, event->timestamp);
		clockData->maxTimestamp = MAX_VALUE(clockData->maxTimestamp, lastEvent->timestamp);
	}
}

void readCSVMetricDefs(CSVMetricSource *source){
	uint64_t timerResolution = 1000000; //fixed value
	uint64_t realtimeTimestamp = source->referencetime;
	FILE *fp;
	struct csv_parser p;
	char buf[1024];
	size_t bytes_read;

	OTF2_StringRef emptyWord = 0;
	source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), emptyWord, "");
	OTF2_StringRef treeNodeRootName = emptyWord;
	OTF2_StringRef className = emptyWord;
	OTF2_SystemTreeNodeRef rootNode = 0;
	source->callbackHolder.onGlobalDef_SystemTreeNode((void *)&(source->callbackHolder), rootNode, treeNodeRootName, className, OTF2_UNDEFINED_SYSTEM_TREE_NODE);
	OTF2_SystemTreeNodeRef scope = 1;
	source->callbackHolder.onGlobalDef_SystemTreeNode((void *)&(source->callbackHolder), scope, treeNodeRootName, className, rootNode);
	//LOCATION_GROUP
	OTF2_LocationGroupRef locationGroup = 0;
	OTF2_StringRef locationGroupName = emptyWord;
	OTF2_LocationGroupType locationGroupType = OTF2_LOCATION_GROUP_TYPE_PROCESS;
	source->callbackHolder.onGlobalDef_LocationGroup((void *)&(source->callbackHolder), locationGroup, locationGroupName, locationGroupType, scope, OTF2_UNDEFINED_LOCATION_GROUP );
	OTF2_GroupRef group = 0;
	OTF2_StringRef groupName = emptyWord;
	OTF2_GroupType groupType = OTF2_GROUP_TYPE_COMM_LOCATIONS;
	OTF2_Paradigm paradigm = OTF2_PARADIGM_UNKNOWN;
	OTF2_GroupFlag groupFlags = OTF2_GROUP_FLAG_NONE;
	uint32_t numberOfMembers = 0;
	const uint64_t* members = NULL;
	source->callbackHolder.onGlobalDef_Group((void *)&(source->callbackHolder), group, groupName, groupType, paradigm, groupFlags, numberOfMembers, members);
	if (csv_init(&p, CSV_APPEND_NULL) != 0) exit(EXIT_FAILURE);
	csv_set_delim(&p, source->propertyseparator);
	fp = fopen(source->path, "rb");
	if (!fp) exit(EXIT_FAILURE);

	while ((bytes_read=fread(buf, 1, 1024, fp)) > 0)
	  if (csv_parse(&p, buf, bytes_read, afterField_cb, afterItem_cb, source) != bytes_read) {
	    fprintf(stderr, "Error while parsing file: %s\n",
	    csv_strerror(csv_error(&p)) );
	    exit(EXIT_FAILURE);
	  }

	csv_fini(&p, afterField_cb, afterItem_cb, &source);

	fclose(fp);

	for(unsigned i = 0; i < source->dataFieldIndexCount; i++){
		free(source->valueNames[i]);
	}
	free(source->dataFieldIndexes);
	free(source->unitNames);
	free(source->valueNames);
	csv_free(&p);
	clockData_t clockData;
	clockData.minTimestamp = UINT64_MAX;
	clockData.maxTimestamp = 0;

	map_iterate(&(source->locationEvents), iterate_locationEvents_for_clockdata, (void *)&clockData);

	source->callbackHolder.onGlobalDef_ClockProperties((void *)&(source->callbackHolder), timerResolution, clockData.minTimestamp, clockData.maxTimestamp - clockData.minTimestamp, realtimeTimestamp );
	source->callbackHolder.onDefFinished(&(source->callbackHolder));
}

static void iterate_locationEvents(void *from, void *to, void *additionalData){
	CSVMetricSource *source = (CSVMetricSource *) additionalData;
	OTF2_LocationRef location = AS_LOCATION_REF(from);
	list *eventList = (list *)to;
	OTF2_AttributeList *attributeList = OTF2_AttributeList_New();
	for(int j = 0; j < eventList->entryCount; j++){
		MetricEvent *event = (MetricEvent *)eventList->entries[j];
		OTF2_MetricRef metricInstance = 2 * source->currentRow + 1;
		OTF2_Type type = OTF2_TYPE_DOUBLE;
		double val = event->value;
		source->callbackHolder.onEvt_Metric(location, event->timestamp, j, &(source->callbackHolder), attributeList, metricInstance, 1, &type, (const OTF2_MetricValue *)&val);
	}
	OTF2_AttributeList_Delete(attributeList);
	source->currentRow++;
}

void readCSVMetricTimedData(CSVMetricSource *source){
	source->currentRow = 0;
	map_iterate(&(source->locationEvents), iterate_locationEvents, (void *)source);
	source->callbackHolder.onTimedDataFinished(&(source->callbackHolder));
}

static void iterate_selector(void *from, void *to, void *additionalData){
	CSVMetricSource *source = (CSVMetricSource *) additionalData;
	uint64_t fieldIndex = (uint64_t) (intptr_t) from;
	char *unit = (char *) to;
	source->dataFieldIndexes[source->currentRow] = fieldIndex;
	source->unitNames[source->currentRow] = unit;
	source->valueNames[source->currentRow] = NULL;
	source->currentRow++;
}

void transformIntoDefaultCallbackHolder(CallbackHolder *callbackHolder);

Pipeline *createCSVMetricSource(const char *path, const char *itemseparator, const char *propertyseparator, uint64_t time, uint64_t resolution, uint64_t referencetime, map *selector, bool useIsoTime){
	failOnFileMissing(path);
	CSVMetricSource *source = (CSVMetricSource *)malloc(sizeof(CSVMetricSource));
	if(NULL != source){
		source->readDefsCallback = (ReadDefsCallback)readCSVMetricDefs;
		source->readTimedDataCallback = (ReadTimedDataCallback)readCSVMetricTimedData;
		unsigned fieldLen = map_entry_count(selector);
		source->resolutionFactor = 1000000.0/resolution;
		source->referencetime = referencetime;
		source->currentField = 0;
		source->currentRow = 0;
		source->currentFieldIndex = 0;
		source->currentTimestamp = 0;
		source->currentValue = 0.0;
		source->timestampFieldIndex = time;
		source->valueNames = (char **)calloc(fieldLen, sizeof(char *));
		source->dataFieldIndexes = (uint64_t *)calloc(fieldLen, sizeof(uint64_t));
		source->dataFieldIndexCount = fieldLen;
		source->unitNames = (char **)calloc(fieldLen, sizeof(char *));
		map_iterate(selector, iterate_selector, (void *)source);
		source->currentRow = 0;
		source->path = path;
		source->propertyseparator = propertyseparator[0];
		if(0 == strcmp("\\n", itemseparator)){
			source->itemseparator = '\n';
		} else if(0 == strcmp("\\r", itemseparator)){
			source->itemseparator = '\r';
		} else {
			source->itemseparator = itemseparator[0];
		}
		source->usesIsoTime = useIsoTime;
		map_initialize(&(source->locationEvents), compareMapInt, hashUint32);

		transformIntoDefaultCallbackHolder(&(source->callbackHolder));
	}
	return asPipeline((CallbackHolder *)source, (ReleaseCallback)releaseCSVMetricSource);
}
