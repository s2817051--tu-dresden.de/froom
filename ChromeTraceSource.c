/* Author: Jan Frenzel */
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "CallbackHolder.h"
#include "list.h"
#include "AbstractSource.internal.h"
#include "Pipelines.h"
#include "Pipelines.internal.h"
#include <jansson.h>

typedef struct _ChromeTraceSource ChromeTraceSource;
struct _ChromeTraceSource{
	ABSTRACT_SOURCE_MEMBERS
	const char *path;
	json_t *jsonRoot;
	map processMap; /* int -> process */
	map functionMap; /* char * -> region */
	map metricMap; /* char * -> metric */
	json_t *dataflowStart; /* event */
	list communicators; /* communicator */
	list durationEvents; /* durationevent */
	list flowEvents; /* mapentry (event, event) */
	map locationEvents; /* location -> list(event) */
	OTF2_StringRef nextStringRef;
	OTF2_LocationGroupRef nextLocationGroupRef;
	OTF2_LocationRef nextLocationRef;
	OTF2_RegionRef nextRegionRef;
	OTF2_GroupRef nextGroupRef;
	OTF2_CommRef nextCommRef;
};

typedef struct _DurationEvent DurationEvent;
struct _DurationEvent{
	bool is_begin;
	uint64_t time;
	int pid;
	int tid;
	const char *name;
};

typedef struct _Location Location;
struct _Location{
	char *name;
	OTF2_LocationRef location;
};

static void releaseLocation(Location *location){
	free(location->name);
	free(location);
}

typedef struct _Process Process;
struct _Process{
	char *name;
	OTF2_LocationGroupRef group;
	map threads; /* tid -> Location */
};

static void del_process_threads_entry(void *from, void *to){
	Location *location = (Location *)to;
	releaseLocation(location);
}

static void releaseProcess(Process *process){
	free(process->name);
	map_delete(&(process->threads), del_process_threads_entry);
	free(process);
}

static void del_process_map_entry(void *from, void *to){
	Process *process = (Process *)to;
	releaseProcess(process);
}

static void del_location_events(void *element){
	free(element);
}

static void del_location_events_list(void *from, void *to){
	list *theList = (list *)to;
	list_delete(theList, del_location_events);
	free(theList);
}

static void releaseChromeTraceSource(ChromeTraceSource *source){
	map_delete(&(source->processMap), del_process_map_entry);
	map_delete(&(source->functionMap), del_func_nothing);
	map_delete(&(source->metricMap), del_func_nothing);
	list_delete(&(source->communicators), del_list_element_func_free);
	list_delete(&(source->durationEvents), del_list_element_func_free);
	list_delete(&(source->flowEvents), del_list_element_func_free);
	map_delete(&(source->locationEvents), del_location_events_list);
	json_decref(source->jsonRoot);
	free(source);
}

static uint64_t convert_time_to_ticks(double val){
	return (uint64_t)(val*1000);
}

static double getDoubleFromJsonObject(json_t *obj){
	double toReturn = 0;
	if(json_is_string(obj)){
		const char *stringVal = json_string_value(obj);
		sscanf(stringVal, "%lf", &toReturn);
	}else{
		toReturn = json_number_value(obj);
	}
	return toReturn;
}

static uint64_t getDoubleFromJson(json_t *event, const char *field){
	return getDoubleFromJsonObject(json_object_get(event, field));
}

static double getIntFromJsonObject(json_t *obj){
	uint64_t toReturn = 0;
	if(json_is_string(obj)){
		const char *stringVal = json_string_value(obj);
		sscanf(stringVal, "%"SCNu64, &toReturn);
	}else{
		toReturn = json_integer_value(obj);
	}
	return toReturn;
}

static uint64_t getIntFromJson(json_t *event, const char *field){
	return getIntFromJsonObject(json_object_get(event, field));
}

static DurationEvent *createDurationEvent(json_t *event, uint64_t timestamp, const char *type){
	DurationEvent *durationEvent = (DurationEvent *)malloc(sizeof(DurationEvent));
	if(NULL != durationEvent){
		durationEvent->is_begin = (0 == strncmp("B", type, 1));
		durationEvent->time = timestamp;
		durationEvent->pid = getIntFromJson(event, "pid");
		durationEvent->tid = getIntFromJson(event, "tid");
		durationEvent->name = json_string_value(json_object_get(event, "name"));
	}
	return durationEvent;
}

static void handleDuration(json_t *event, ChromeTraceSource *source){
	list_add(&(source->durationEvents), createDurationEvent(event, convert_time_to_ticks(getDoubleFromJson(event, "ts")), json_string_value(json_object_get(event, "ph"))));
}

static void handleComplete(json_t *event, ChromeTraceSource *source){
	uint64_t beginTs = 0;
	uint64_t endTs = 0;
	uint64_t duration = 0;
	json_t * args = json_object_get(event, "args");
	if(NULL != args){
		json_t *beginNs = json_object_get(args, "BeginNs");
		if(NULL != beginNs){
			beginTs = getDoubleFromJsonObject(beginNs);
		}else{
			beginTs = convert_time_to_ticks(getDoubleFromJson(event, "ts"));
		}
		json_t *endNs = json_object_get(args, "EndNs");
		if(NULL != endNs){
			endTs = getDoubleFromJsonObject(endNs);
		}else{
			duration = getDoubleFromJson(event, "dur");
			endTs = convert_time_to_ticks(getDoubleFromJson(event, "ts")+duration);
		}
	}else{
		beginTs = convert_time_to_ticks(getDoubleFromJson(event, "ts"));
		duration = getDoubleFromJson(event, "dur");
		endTs = convert_time_to_ticks(getDoubleFromJson(event, "ts")+duration);
	}
	list_add(&(source->durationEvents), createDurationEvent(event, beginTs, "B"));
	list_add(&(source->durationEvents), createDurationEvent(event, endTs, "E"));
}

static Process *addProcess(ChromeTraceSource *source, int pid, const char *name){
	char *processName;
	OTF2_LocationGroupRef group;
	if(NULL == name){
		processName = calloc(5, sizeof(char));
		memset(processName, 0, 5);
		snprintf(processName, 5, "%d",pid);
	}else{
		processName = strdup(name);
	}
	OTF2_StringRef processNameRef = source->nextStringRef++;
	source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), processNameRef, processName);
	group = source->nextLocationGroupRef++;
	OTF2_LocationGroupType locationGroupType = OTF2_LOCATION_GROUP_TYPE_PROCESS;
	OTF2_SystemTreeNodeRef scope = 1;
	source->callbackHolder.onGlobalDef_LocationGroup((void *)&(source->callbackHolder), group, processNameRef, locationGroupType, scope, OTF2_UNDEFINED_LOCATION_GROUP );
	mapentry *entry = NULL;
	Process *process = NULL;
	if((entry = map_find(&(source->processMap),AS_VOID_PTR(pid))) == NULL){
		process = (Process *)malloc(sizeof(Process));
		map_add(&(source->processMap), AS_VOID_PTR(pid), process);
		map_initialize(&(process->threads), compareMapInt, hashUint32);
	}else{
		process = (Process *)entry->to;
		free(process->name);
	}
	process->name = processName;
	process->group = group;
	return process;
}

static Location *addThread(ChromeTraceSource *source, Process *process, int tid, const char *name){
	char *threadName;
	Location *location;
	OTF2_LocationRef locationRef;
	if(NULL == name){
		int len = strlen(process->name) + 10; // assuming a space, tid and '\0' are never longer than 10
		threadName = calloc(len, sizeof(char));
		memset(threadName, 0, len);
		snprintf(threadName, len, "%s %d", process->name, tid);
	}else{
		threadName = strdup(name);
	}
	locationRef = source->nextLocationRef++;
	location = (Location *)malloc(sizeof(Location));
	location->name = threadName;
	location->location = locationRef;
	map_add(&(process->threads), AS_VOID_PTR(tid), location);
	mapentry *entry = NULL;
	list *eventList;
	if((entry = map_find(&(source->locationEvents),AS_VOID_PTR(locationRef))) == NULL){
		eventList = (list *)malloc(sizeof(list));
		list_initialize(eventList);
		map_add(&(source->locationEvents), AS_VOID_PTR(locationRef), eventList);
	}
	return location;
}

static Location *getLocation(ChromeTraceSource *source, int pid, int tid){
	mapentry *entry = NULL;
	Process *process = NULL;
	Location *location = NULL;
	if((entry = map_find(&(source->processMap),AS_VOID_PTR(pid))) == NULL){
		process = addProcess(source, pid, NULL);
	}else{
		process = (Process *)entry->to;
	}
	if((entry = map_find(&(process->threads),AS_VOID_PTR(tid))) == NULL){
		location = addThread(source, process, tid, NULL);
	}else{
		location = (Location *)entry->to;
	}
	return location;
}

static Location *getLocationFromEvent(ChromeTraceSource *source, json_t *event){
	int pid = getIntFromJson(event, "pid");
	int tid = getIntFromJson(event, "tid");
	return getLocation(source, pid, tid);
}

static OTF2_MetricRef addMetric(ChromeTraceSource *source, const char *metricName, const char *metricUnit){
	//LOCATION
	OTF2_StringRef emptyWord = 0;
	OTF2_LocationRef location = source->nextLocationRef++;
	OTF2_LocationGroupRef locationGroup = 0;
	uint64_t numberOfEvents = 0;
	source->callbackHolder.onGlobalDef_Location((void *)&(source->callbackHolder), location, emptyWord, OTF2_LOCATION_TYPE_METRIC, numberOfEvents, locationGroup );
	// we save metrics in a map, so use it to create id
	int metricCount = map_entry_count(&(source->metricMap));
	//METRIC_MEMBER
	OTF2_StringRef unitRef = source->nextStringRef++; //TODO: this duplicates "Bytes"
	source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), unitRef, metricUnit);
	OTF2_MetricMemberRef metricMember = metricCount;
	int64_t exponent = 0;
	OTF2_StringRef nameRef = source->nextStringRef++;
	OTF2_StringRef description = emptyWord;
	source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), nameRef, metricName);
	source->callbackHolder.onGlobalDef_MetricMember((void *)&(source->callbackHolder), metricMember, nameRef, description, OTF2_METRIC_TYPE_OTHER, OTF2_METRIC_ABSOLUTE_POINT, OTF2_TYPE_DOUBLE, OTF2_BASE_DECIMAL, exponent, unitRef);
	//METRIC_CLASS
	OTF2_MetricRef metricClass = 2 * metricCount;
	source->callbackHolder.onGlobalDef_MetricClass((void *)&(source->callbackHolder), metricClass, 1, &metricMember, OTF2_METRIC_ASYNCHRONOUS, OTF2_RECORDER_KIND_ABSTRACT);
	//METRIC_INSTANCE
	OTF2_MetricRef metricInstance = 2 * metricCount + 1;
	uint64_t scope = 1;
	source->callbackHolder.onGlobalDef_MetricInstance((void *)&(source->callbackHolder), metricInstance, metricClass, location, OTF2_SCOPE_SYSTEM_TREE_NODE, scope);

	map_add(&(source->metricMap), metricName, AS_VOID_PTR(metricInstance));
	return metricInstance;
}

static void handleCounter(json_t *event, ChromeTraceSource *source){
	const char *metricName = json_string_value(json_object_get(event, "name"));
	if(0 == strcmp(metricName, "Allocated Bytes")){
		OTF2_MetricRef metric;
		mapentry *entry;
		if((entry = map_find(&(source->metricMap),metricName)) == NULL){
			metric = addMetric(source, metricName, "Bytes");
		}else{
			metric = AS_METRIC_REF(entry->to);
		}
		json_t *args = json_object_get(event, "args");
		double metricValue = json_real_value(json_object_get(args, "Allocator Bytes in Use"));
		uint64_t timestamp = convert_time_to_ticks(getDoubleFromJson(event, "ts"));
		OTF2_LocationRef location = getLocationFromEvent(source, event)->location;
		OTF2_AttributeList *attributeList = OTF2_AttributeList_New();
		OTF2_Type type = OTF2_TYPE_DOUBLE;
		uint64_t eventPosition = 0;
		source->callbackHolder.onEvt_Metric(location, timestamp, eventPosition, &(source->callbackHolder), attributeList, metric, 1, &type, (const OTF2_MetricValue *)&metricValue);
	}
}

static void handleFlowStart(json_t *event, ChromeTraceSource *source){
	source->dataflowStart = event;
}

static void handleFlowStep(json_t *event, ChromeTraceSource *source){
	mapentry * entry = (mapentry *)malloc(sizeof(mapentry));
	entry->from = (void *)source->dataflowStart;
	entry->to = (void *) event;
	list_add(&(source->flowEvents), entry);
	source->dataflowStart = NULL;
}

static void handleMetadata(json_t *event, ChromeTraceSource *source){
	json_t *name = json_object_get(event, "name");
	const char *eventType = json_string_value(name);
	if(0 == strcmp(eventType, "process_name")){
		json_t *pidNode = json_object_get(event, "pid");
		int pid = json_integer_value(pidNode);
		json_t *args = json_object_get(event, "args");
		json_t *nameNode = json_object_get(args, "name");
		const char *name = json_string_value(nameNode);
		addProcess(source, pid, name);
	}else if(0 == strcmp(eventType, "thread_name")){
		json_t *pidNode = json_object_get(event, "pid");
		int pid = json_integer_value(pidNode);
		json_t *tidNode = json_object_get(event, "tid");
		int tid = json_integer_value(tidNode);
		json_t *args = json_object_get(event, "args");
		json_t *nameNode = json_object_get(args, "name");
		const char *name = json_string_value(nameNode);
		mapentry *entry;
		Process *process;
		if((entry = map_find(&(source->processMap),AS_VOID_PTR(pid))) == NULL){
			process = addProcess(source, pid, NULL);
		}else{
			process = (Process *)entry->to;
		}
		addThread(source, process, tid, name);
	} else {
		printf("Unknown metadata event: %s\n", eventType);
	}
}

static int timeCompareDurationEvent(const void *first, const void *second){
	DurationEvent *firstEvent = *(DurationEvent **)first;
	DurationEvent *secondEvent = *(DurationEvent **)second;
	if(firstEvent->time < secondEvent->time){
		return -1;
	}else if(firstEvent->time > secondEvent->time){
		return 1;
	}
	return 0;
}

#define TYPE_ENTER 'B'
#define TYPE_LEAVE 'E'
#define TYPE_SEND 'S'
#define TYPE_RECEIVE 'R'
typedef struct _MessageData MessageData;
struct _MessageData{
	uint32_t rank; // sender/receiver
	OTF2_CommRef communicator;
	uint32_t messageTag;
};


typedef struct _SavedEvent SavedEvent;
struct _SavedEvent{
	char type;
	uint64_t timestamp;
	union {
		OTF2_RegionRef region;
		MessageData messageData;
	} payload;
};


static SavedEvent *createSavedEvent(char type, uint64_t timestamp){
	SavedEvent *event = (SavedEvent *)malloc(sizeof(SavedEvent));
	event->type = type;
	event->timestamp = timestamp;
	return event;
}

static int getRank(list *theList, OTF2_LocationRef location){
	for(int i = 0; i < theList->entryCount; i++){
		OTF2_LocationRef listLocation = AS_LOCATION_REF(theList->entries[i]);
		if(location == listLocation){
			return i;
		}
	}
	return -1;
}

static bool listHasLocation(list *theList, OTF2_LocationRef location){
	return (-1 != getRank(theList, location));
}

typedef struct _Communicator Communicator;
struct _Communicator{
	const char *name;
	OTF2_LocationRef members[2];
	uint64_t ranks[2];
	OTF2_CommRef communicator;
};

static Communicator *createCommunicator(const char *name, OTF2_LocationRef members[2], uint64_t ranks[2], OTF2_CommRef comm){
	Communicator *communicator = (Communicator *)malloc(sizeof(Communicator));
	communicator->name = name;
	communicator->members[0] = members[0];
	communicator->members[1] = members[1];
	communicator->ranks[0] = ranks[0];
	communicator->ranks[1] = ranks[1];
	communicator->communicator = comm;
	return communicator;
}

static int getRankFromComm(Communicator *communicator, OTF2_LocationRef location){
	if(communicator->members[0] == location){
		return 0;
	}
	return 1;
}

static Communicator *getCommunicator(ChromeTraceSource *source, list *listForRank, const char *name, OTF2_LocationRef sender, OTF2_LocationRef receiver){
	OTF2_LocationRef elements[2];
	uint64_t ranks[2];
	int senderRank = getRank(listForRank, sender);
	int receiverRank = getRank(listForRank, receiver);
	if(sender < receiver){
		elements[0] = sender;
		elements[1] = receiver;
		ranks[0] = senderRank;
		ranks[1] = receiverRank;
	}else{
		elements[0] = receiver;
		elements[1] = sender;
		ranks[0] = receiverRank;
		ranks[1] = senderRank;
	}
	for(int i = 0; i < source->communicators.entryCount; i++){
		Communicator *communicator = (Communicator *)(source->communicators.entries[i]);
		if( 0 == strcmp(name, communicator->name) && elements[0] == communicator->members[0] && elements[1] == communicator->members[1]){
			return communicator;
		}
	}
	OTF2_CommRef comm = source->nextCommRef++;
	Communicator *communicator = createCommunicator(name, elements, ranks, comm);
	list_add(&(source->communicators), communicator);
	return communicator;
}

static int timeCompareSavedEvent(const void *first, const void *second){
	SavedEvent *firstEvent = *(SavedEvent **)first;
	SavedEvent *secondEvent = *(SavedEvent **)second;
	if(firstEvent->timestamp < secondEvent->timestamp){
		return -1;
	}else if(firstEvent->timestamp > secondEvent->timestamp){
		return 1;
	}
	return 0;
}

typedef struct _clockData clockData_t;
struct _clockData{
	uint64_t minTimestamp;
	uint64_t maxTimestamp;
};

static void iterate_locationEvents_for_clockdata(void *from, void *to, void *additionalData){
	clockData_t *clockData = (clockData_t *) additionalData;
	list *eventList = (list *)to;
	qsort(eventList->entries, eventList->entryCount, sizeof(SavedEvent *), timeCompareSavedEvent);
	if(eventList->entryCount > 0){
		clockData->minTimestamp = MIN_VALUE(clockData->minTimestamp, ((SavedEvent *)eventList->entries[0])->timestamp);
		clockData->maxTimestamp = MAX_VALUE(clockData->maxTimestamp, ((SavedEvent *)eventList->entries[eventList->entryCount-1])->timestamp);
	}
}

typedef struct _threadIterateData threadIterateData_t;
struct _threadIterateData{
	ChromeTraceSource *source;
	Process *process;
};

static void iterate_threadMap(void *from, void *to, void *additionalData){
	threadIterateData_t *threadIterateData = (threadIterateData_t *) additionalData;
	ChromeTraceSource *source = threadIterateData->source;
	Location *location = (Location *)to;
	OTF2_StringRef threadNameRef = source->nextStringRef++;
	source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), threadNameRef, location->name);
	uint64_t numberOfEvents = 0;
	mapentry *locationEntry= map_find(&(source->locationEvents),AS_VOID_PTR(location->location));
	list *eventList = (list *)locationEntry->to;
	numberOfEvents = eventList->entryCount;
	source->callbackHolder.onGlobalDef_Location((void *)&(source->callbackHolder), location->location, threadNameRef, OTF2_LOCATION_TYPE_CPU_THREAD, numberOfEvents, threadIterateData->process->group );
}

static void iterate_processMap(void *from, void *to, void *additionalData){
	ChromeTraceSource *source = (ChromeTraceSource *) additionalData;
	Process *process = (Process *) to;
	threadIterateData_t threadIterateData;
	threadIterateData.source = source;
	threadIterateData.process = process;
	map_iterate(&(process->threads), iterate_threadMap, (void *)&threadIterateData);
}

static void readChromeTraceDefs(ChromeTraceSource *source){
	uint64_t timerResolution = 1000000000; //fixed value
	uint64_t realtimeTimestamp = 0;
	OTF2_StringRef emptyWord = source->nextStringRef++;
	source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), emptyWord, "");
	OTF2_StringRef rootNodeString = source->nextStringRef++;
	source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), rootNodeString, "root node");
	OTF2_StringRef hostString = source->nextStringRef++;
	source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), hostString, "myHost");
	OTF2_StringRef className = emptyWord;
	OTF2_SystemTreeNodeRef rootNode = 0;
	source->callbackHolder.onGlobalDef_SystemTreeNode((void *)&(source->callbackHolder), rootNode, rootNodeString, className, OTF2_UNDEFINED_SYSTEM_TREE_NODE);
	OTF2_SystemTreeNodeRef scope = 1;
	source->callbackHolder.onGlobalDef_SystemTreeNode((void *)&(source->callbackHolder), scope, hostString, className, rootNode);

	json_t *event_array = json_object_get(source->jsonRoot, "traceEvents");
	for(size_t i = 0; i < json_array_size(event_array); i++){
		json_t *event = json_array_get(event_array, i);
		json_t *phase = json_object_get(event, "ph");
		if(NULL != phase){
			const char *val = json_string_value(phase);
			switch(*val){
			case 'B':
			case 'E':
				handleDuration(event, source);
				break;
			case 'X':
				handleComplete(event, source);
				break;
			case 'C':
				handleCounter(event, source);
				break;
			case 's':
				handleFlowStart(event, source);
				break;
			case 't':
				handleFlowStep(event, source);
				break;
			case 'M':
				handleMetadata(event, source);
				break;

			default:
				printf("Unhandled event type: %c\n", *val);
			};
		}
	}
	qsort(source->durationEvents.entries, source->durationEvents.entryCount, sizeof(DurationEvent *), timeCompareDurationEvent);
	for(int i = 0; i < source->durationEvents.entryCount; i++){
		DurationEvent *event = (DurationEvent *)(source->durationEvents.entries[i]);
		mapentry *entry = NULL;
		OTF2_RegionRef function;
		if((entry = map_find(&(source->functionMap),event->name)) == NULL){
			OTF2_StringRef emptyWord = 0;
			OTF2_StringRef regionName = source->nextStringRef++;
			source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), regionName, event->name);
			function = source->nextRegionRef++;
			OTF2_StringRef description = emptyWord;
			OTF2_StringRef sourceFile = OTF2_UNDEFINED_STRING;
			int beginLineNumber = 0;
			int endLineNumber = 0;
			source->callbackHolder.onGlobalDef_Region( (void *)&(source->callbackHolder), function, regionName, regionName, description, OTF2_REGION_ROLE_FUNCTION, OTF2_PARADIGM_USER, OTF2_REGION_FLAG_NONE, sourceFile, beginLineNumber, endLineNumber);
			map_add(&(source->functionMap), event->name, AS_VOID_PTR(function));
		}else{
			function = AS_REGION_REF(entry->to);
		}
		OTF2_LocationRef location = getLocation(source, event->pid, event->tid)->location;
		entry = map_find(&(source->locationEvents),AS_VOID_PTR(location));
		list *eventList = (list *)(entry->to);
		SavedEvent *savedEvent;
		if(event->is_begin){
			savedEvent = createSavedEvent(TYPE_ENTER, event->time);
		}else{
			savedEvent = createSavedEvent(TYPE_LEAVE, event->time);
		}
		savedEvent->payload.region = function;
		list_add(eventList, savedEvent);
	}
	list dataFlowLocations;
	list_initialize(&dataFlowLocations);
	for(int i = 0; i < source->flowEvents.entryCount; i++){
		mapentry *entry = (mapentry *)(source->flowEvents.entries[i]);
		json_t *event1 = (json_t *)entry->from;
		json_t *event2 = (json_t *)entry->to;
		OTF2_LocationRef location1 = getLocationFromEvent(source, event1)->location;
		OTF2_LocationRef location2 = getLocationFromEvent(source, event2)->location;
		if(!listHasLocation(&dataFlowLocations, location1)){
			list_add(&dataFlowLocations, AS_VOID_PTR((uint64_t)location1));
		}
		if(!listHasLocation(&dataFlowLocations, location2)){
			list_add(&dataFlowLocations, AS_VOID_PTR((uint64_t)location2));
		}
	}
	for(int i = 0; i < source->flowEvents.entryCount; i++){
		mapentry *entry = (mapentry *)(source->flowEvents.entries[i]);
		json_t *event1 = (json_t *)entry->from;
		json_t *event2 = (json_t *)entry->to;
		OTF2_LocationRef location1 = getLocationFromEvent(source, event1)->location;
		OTF2_LocationRef location2 = getLocationFromEvent(source, event2)->location;
		SavedEvent *savedEvent;
		const char *commName = json_string_value(json_object_get(event1, "cat"));
		Communicator *communicator = getCommunicator(source, &(dataFlowLocations), commName, location1, location2);
		uint64_t timestamp = convert_time_to_ticks(getDoubleFromJson(event1, "ts"));
		savedEvent = createSavedEvent(TYPE_SEND, timestamp);
		savedEvent->payload.messageData.rank = getRankFromComm(communicator, location2);;
		savedEvent->payload.messageData.communicator = communicator->communicator;
		savedEvent->payload.messageData.messageTag = (uint32_t)json_integer_value(json_object_get(event1, "id"));
		entry = map_find(&(source->locationEvents),AS_VOID_PTR(location1));
		list *eventList;
		eventList = (list *)(entry->to);
		list_add(eventList, savedEvent);

		timestamp = convert_time_to_ticks(getDoubleFromJson(event2, "ts"));
		savedEvent = createSavedEvent(TYPE_RECEIVE, timestamp);
		savedEvent->payload.messageData.rank = getRankFromComm(communicator, location1);;
		savedEvent->payload.messageData.communicator = communicator->communicator;
		savedEvent->payload.messageData.messageTag = (uint32_t)json_integer_value(json_object_get(event2, "id"));
		entry = map_find(&(source->locationEvents),AS_VOID_PTR(location2));
		eventList = (list *)(entry->to);
		list_add(eventList, savedEvent);
	}

	// deferred writing of definitions
	clockData_t clockData;
	clockData.minTimestamp = UINT64_MAX;
	clockData.maxTimestamp = 0;

	map_iterate(&(source->locationEvents), iterate_locationEvents_for_clockdata, (void *)&clockData);
	source->callbackHolder.onGlobalDef_ClockProperties((void *)&(source->callbackHolder), timerResolution, clockData.minTimestamp, clockData.maxTimestamp - clockData.minTimestamp, realtimeTimestamp );
	map_iterate(&(source->processMap), iterate_processMap, (void *)source);
	OTF2_GroupRef group = source->nextGroupRef++;
	OTF2_StringRef groupName = source->nextStringRef++;
	source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), groupName, "DataFlow (COMM_LOCATIONS)");
	OTF2_GroupType groupType = OTF2_GROUP_TYPE_COMM_LOCATIONS;
	OTF2_Paradigm paradigm = OTF2_PARADIGM_NONE;
	OTF2_GroupFlag groupFlags = OTF2_GROUP_FLAG_NONE;
	uint32_t numberOfMembers = dataFlowLocations.entryCount;
	const uint64_t* members = (const uint64_t *)dataFlowLocations.entries;
	source->callbackHolder.onGlobalDef_Group((void *)&(source->callbackHolder), group, groupName, groupType, paradigm, groupFlags, numberOfMembers, members);
	list_delete(&dataFlowLocations, del_list_element_func_nothing);
	for(int i = 0; i < source->communicators.entryCount; i++){
		Communicator *communicator = (Communicator *)source->communicators.entries[i];
		const char *name = communicator->name;
		OTF2_GroupRef group = source->nextGroupRef++;
		OTF2_StringRef groupName = source->nextStringRef++;
		OTF2_CommRef comm = communicator->communicator;
#define APPEND_STR " COMM_GROUP"
#define APPEND_STR_LEN strlen(APPEND_STR)
		char *commGroupName = (char *) malloc(strlen(name)+APPEND_STR_LEN+1);
		memset(commGroupName, 0, strlen(name)+APPEND_STR_LEN+1);
		sprintf(commGroupName, "%s%s", name, APPEND_STR);
		source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), groupName, commGroupName);
		free(commGroupName);
		OTF2_GroupType groupType = OTF2_GROUP_TYPE_COMM_GROUP;
		OTF2_Paradigm paradigm = OTF2_PARADIGM_NONE;
		OTF2_GroupFlag groupFlags = OTF2_GROUP_FLAG_NONE;
		uint32_t numberOfMembers = 2;
		const uint64_t* members = communicator->ranks;
		source->callbackHolder.onGlobalDef_Group((void *)&(source->callbackHolder), group, groupName, groupType, paradigm, groupFlags, numberOfMembers, members);
		OTF2_StringRef commName = source->nextStringRef++;
		OTF2_CommFlag flags = OTF2_COMM_FLAG_NONE;
		source->callbackHolder.onGlobalDef_String((void *)&(source->callbackHolder), commName, name);
		source->callbackHolder.onGlobalDef_Comm( (void *)&(source->callbackHolder),comm,commName,group,OTF2_UNDEFINED_COMM, flags );
	}
	source->callbackHolder.onDefFinished(&(source->callbackHolder));
}

static void iterate_locationEvents(void *from, void *to, void *additionalData){
	ChromeTraceSource *source = (ChromeTraceSource *) additionalData;
	OTF2_LocationRef location = AS_LOCATION_REF(from);
	list *eventList = (list *)to;
	for(int j = 0; j < eventList->entryCount; j++){
		SavedEvent *event = (SavedEvent *)eventList->entries[j];
		OTF2_AttributeList *attributeList = OTF2_AttributeList_New();
		uint64_t eventPosition = 0;
		switch(event->type){
		case TYPE_ENTER:
			source->callbackHolder.onEvt_Enter(location, event->timestamp, eventPosition, &(source->callbackHolder), attributeList, event->payload.region);
			break;
		case TYPE_LEAVE:
			source->callbackHolder.onEvt_Leave(location, event->timestamp, eventPosition, &(source->callbackHolder), attributeList, event->payload.region);
			break;
		case TYPE_SEND:
			source->callbackHolder.onEvt_MpiSend(location, event->timestamp, eventPosition, &(source->callbackHolder), attributeList, event->payload.messageData.rank, event->payload.messageData.communicator, event->payload.messageData.messageTag, 0);
			break;
		case TYPE_RECEIVE:
			source->callbackHolder.onEvt_MpiRecv(location, event->timestamp, eventPosition, &(source->callbackHolder), attributeList, event->payload.messageData.rank, event->payload.messageData.communicator, event->payload.messageData.messageTag, 0);
			break;
		}
		OTF2_AttributeList_Delete(attributeList);
	}
}

static void readChromeTraceTimedData(ChromeTraceSource *source){
	map_iterate(&(source->locationEvents), iterate_locationEvents, (void *)source);
	source->callbackHolder.onTimedDataFinished(&(source->callbackHolder));
}

struct json_read_state{
	char *data;
	size_t capacity;
	int pos;
};

static void addToBuffer(struct json_read_state *buffer, void * toAdd, size_t toAddCount){
	if(buffer->pos + toAddCount >= buffer->capacity){
		size_t newCapacity = buffer->capacity << 1;
		char *newbuffer = realloc(buffer->data, newCapacity);
		if(NULL == newbuffer){
			fputs("Not enough buffer\n", stderr);
			exit(EXIT_FAILURE);
		}
		buffer->data = newbuffer;
		buffer->capacity = newCapacity;
	}
	memcpy(buffer->data + buffer->pos, toAdd, toAddCount);
	buffer->pos += toAddCount;
}

void transformIntoDefaultCallbackHolder(CallbackHolder *callbackHolder);

Pipeline *createChromeTraceSource(const char *path){
	failOnFileMissing(path);
	ChromeTraceSource *source = (ChromeTraceSource *)malloc(sizeof(ChromeTraceSource));
	if(NULL != source){
		source->readDefsCallback = (ReadDefsCallback)readChromeTraceDefs;
		source->readTimedDataCallback = (ReadTimedDataCallback)readChromeTraceTimedData;
		source->path = path;
		source->nextStringRef = 0;
		source->nextLocationGroupRef = 0;
		source->nextLocationRef = 0;
		source->nextRegionRef = 0;
		source->nextGroupRef = 0;
		source->nextCommRef = 0;
		map_initialize(&(source->processMap), compareMapInt, hashUint32);
		map_initialize(&(source->functionMap), COMPARE_TYPE(String), hashString);
		map_initialize(&(source->metricMap), COMPARE_TYPE(String), hashString);
		list_initialize(&(source->communicators));
		list_initialize(&(source->durationEvents));
		list_initialize(&(source->flowEvents));
		map_initialize(&(source->locationEvents), compareMapInt, hashUint32);
		FILE *fp;
		struct json_read_state buffer;
		char buf[1024];
		size_t bytes_read;

		fp = fopen(source->path, "rb");
		if (!fp) exit(EXIT_FAILURE);
		char *data = NULL;
		size_t capacity = 1024;
		data = malloc(capacity);
		buffer.data = data;
		buffer.capacity = capacity;
		buffer.pos = 0;

		while ((bytes_read=fread(buf, 1, 1024, fp)) > 0){
			addToBuffer(&buffer, buf, bytes_read);
		}

		fclose(fp);

		buffer.data[buffer.pos] = '\0';

		json_t *root;
		json_error_t error;
		root = json_loads(buffer.data, 0, &error);
		free(buffer.data);
		if (!root) {
			fprintf(stderr, "json error: on line %d: %s\n", error.line, error.text);
			exit(EXIT_FAILURE);
		}
		source->jsonRoot = root;

		transformIntoDefaultCallbackHolder(&(source->callbackHolder));
	}
	return asPipeline((CallbackHolder *)source, (ReleaseCallback)releaseChromeTraceSource);
}


