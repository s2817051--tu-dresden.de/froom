#Author: Jan Frenzel
.SUFFIXES:
include generator/otf2paths
CC = gcc
CFLAGS =-I${OTF2_INCLUDE_DIR} -g -O2 -Wall -Wpedantic -DYYDEBUG=1 -fPIC
OTF2_INCLUDE_DIR=${OTF2_BASE}/include
OTF2_LIB_DIR=${OTF2_BASE}/lib
LIBS=-lotf2 -lm -lcsv -ljansson -pthread
LDFLAGS=-L${OTF2_LIB_DIR}
GENERATED_FILES=Reader.generated.c Writer.generated.c TranslateDefs.c.inc TranslateEvents.c.inc translator.c.inc Unifier.generated.c TimeSlicer.generated.c MyDecl.c.inc LocationRemover.generated.c MessageAdaptor.generated.c CallbackHolder.h
OBJ_FILES=Pipelines.o map.o list.o stable_marriage.o Writer.generated.o TimeSlicer.generated.o RegionRemover.o LocationRemover.generated.o Renamer.o CSVMetricSource.o Reader.generated.o ChromeTraceSource.o MessageAdaptor.generated.o Unifier.generated.o Sequencer.o
PROGRAMS=froom-rename froom-unify froom-slice froom-remove-location froom-remove-region froom-from-csv-metric froom-from-chrome-trace froom-merge-messages
.PHONY: clean run
.KEEP: $(GENERATED_FILES)

all: froom-interpreter libfroom.so $(PROGRAMS)

froom-interpreter: froom_interpreter.c froom_parser.c froom_lexer.c $(OBJ_FILES)
	$(CC) $(CFLAGS) $^ ${LDFLAGS} ${LIBS} -o$@

libfroom.so: $(OBJ_FILES)
	$(CC) $(CFLAGS) -shared -fPIC $^ -o$@ $(LDFLAGS) $(LIBS)

froom-%: froom_%.c $(OBJ_FILES)
	$(CC) $(CFLAGS) $^ ${LDFLAGS} ${LIBS} -o$@

run: froom-interpreter
	@./$< example.froom master=master/traces.otf2 worker=worker/traces.otf2 unified=unifiedtraces

Pipelines.o: Pipelines.c Makefile $(GENERATED_FILES) utils.h Pipelines.h
	@echo "building $@"
	$(CC) $(CFLAGS) -c $< -o $@

RegionRemover.o: RegionRemover.c CallbackHolder.h
	@echo "building $@"
	$(CC) $(CFLAGS) -c $< -o $@

Renamer.o: Renamer.c CallbackHolder.h
	@echo "building $@"
	$(CC) $(CFLAGS) -c $< -o $@

CSVMetricSource.o: CSVMetricSource.c CallbackHolder.h
	@echo "building $@"
	$(CC) $(CFLAGS) -c $< -o $@

ChromeTraceSource.o: ChromeTraceSource.c CallbackHolder.h
	@echo "building $@"
	$(CC) $(CFLAGS) -c $< -o $@

Sequencer.o: Sequencer.c CallbackHolder.h
	@echo "building $@"
	$(CC) $(CFLAGS) -c $< -o $@

Unifier.generated.c: TranslateDefs.c.inc TranslateEvents.c.inc translator.c.inc MyDecl.c.inc CallbackHolder.h

LocationRemover.generated.c: CallbackHolder.h
MessageAdaptor.generated.c: CallbackHolder.h
Reader.generated.c: CallbackHolder.h
TimeSlicer.generated.c: CallbackHolder.h
Writer.generated.c: CallbackHolder.h

%.generated.o: %.generated.c
	@echo "building $@"
	$(CC) $(CFLAGS) -c $< -o $@

%.o: %.c %.h
	@echo "building $@"
	$(CC) $(CFLAGS) -c $< -o $@

froom_parser.c: froom.y froom_lexer.c
	bison $<

froom_lexer.c: froom.l
	flex $<

generator/otf2paths:
	@if test -z "$(OTF2_BASE)"; then echo "Please provide the OTF2 root directory via 'make OTF2_BASE=...'"; exit 1; fi
	./prepare-once.sh "$(OTF2_BASE)"

%.c.inc: generator/%.templ.c.inc generator/otf2paths
	${OTF2_BASE}/bin/otf2-template $< $@

%.generated.c: generator/%.templ.c.inc generator/otf2paths
	${OTF2_BASE}/bin/otf2-template $< $@

%.h: generator/%.templ.h generator/otf2paths
	${OTF2_BASE}/bin/otf2-template $< $@

clean:
	-rm -rf *.o froom-interpreter froom_parser.h froom_parser.c froom_lexer.h froom_lexer.c $(GENERATED_FILES) $(OBJ_FILES)
