/* Author: Jan Frenzel */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "CallbackHolder.h"
#include <unistd.h>
#include "Pipelines.h"
#include "Pipelines.internal.h"

Pipeline *asPipeline(CallbackHolder *callbackHolder,ReleaseCallback releaseCallback){
	Pipeline *pipeline = (Pipeline *)malloc(sizeof(Pipeline));
	PipelineElem *pipelineElem = (PipelineElem *)malloc(sizeof(PipelineElem));
	pipelineElem->callbackHolder = callbackHolder;
	pipelineElem->releaseCallback = releaseCallback;
	pipelineElem->next = NULL;
	pipelineElem->previous = NULL;
	pipelineElem->alternativePrevious = NULL;
	pipeline->first = pipelineElem;
	pipeline->last = pipelineElem;
	return pipeline;
}

Pipeline *createPipeline(Pipeline *head, Pipeline *tail){
	head->last->callbackHolder->next = tail->first->callbackHolder;
	head->last->next = tail->first;
	tail->first->previous = head->last;
	head->last = tail->last;
	free(tail);
	return head;
}

Pipeline *breakPipeline(Pipeline *pipeline){
	char tempDirTemplate[] = "/tmp/otf2temp.XXXXXX";
	char *tempDir = mkdtemp(tempDirTemplate);
	size_t tempDirLen = strlen(tempDir);
	size_t fileNameLen = strlen("/traces.otf2") + 1;
	size_t sourcePathLen = tempDirLen + fileNameLen;

	createPipeline(pipeline, createSink(tempDir));
	char *sourcePath = (char *)malloc(sourcePathLen);
	strncpy(sourcePath, tempDir, tempDirLen);
	strncpy(sourcePath + tempDirLen, "/traces.otf2", fileNameLen);
	Pipeline *source = createSource(sourcePath);
	free(sourcePath);
	return source;
}

static void releasePipelineElemToSource(PipelineElem *elem){
	if(NULL != elem->previous){
		releasePipelineElemToSource(elem->previous);
	}
	if(NULL != elem->alternativePrevious){
		releasePipelineElemToSource(elem->alternativePrevious);
	}
	elem->releaseCallback(elem->callbackHolder);
	free(elem);
}

void releasePipeline(Pipeline *pipeline){
	releasePipelineElemToSource(pipeline->last);
	free(pipeline);
}

#include "AbstractSource.internal.h"

void failOnFileMissing(const char *filePath) {
	if(isProducedBySink(filePath)){
		return;
	}
	if(access(filePath, R_OK)){
		fprintf(stderr, "Cannot read \"%s\". Exiting!\n", filePath);
		exit(EXIT_FAILURE);
	}
}

typedef void (*SourceFunc)(PipelineElem *);

static void readDefsSourceFunc(PipelineElem *elem){
	AbstractSource *source = (AbstractSource *)elem->callbackHolder;
	source->readDefsCallback(source);
}

static void readTimedDataSourceFunc(PipelineElem *elem){
	AbstractSource *source = (AbstractSource *)elem->callbackHolder;
	source->readTimedDataCallback(source);
}

static void applyToAllSources(PipelineElem *elem, SourceFunc func){
	if(NULL != elem->alternativePrevious){
		applyToAllSources(elem->alternativePrevious, func);
	}
	if(NULL == elem->previous){
		func(elem);
	}else{
		applyToAllSources(elem->previous, func);
	}
}

void executePipeline(Pipeline *pipeline){
	applyToAllSources(pipeline->last, readDefsSourceFunc);
	applyToAllSources(pipeline->last, readTimedDataSourceFunc);
}

static const char *symbolTypeToString(SymbolType type){
	return (type==SYMBOL_PIPELINE)?"Pipeline":"String";
}

struct _symtab
{
	const char *name;
	void *value;
	SymbolType type;
	Symtab *next;
};

void storeSym(Symtab **symtab, const char *name, void *value, SymbolType type){
	Symtab *newElement = (Symtab *)malloc(sizeof (Symtab));
	newElement->name = strdup(name);
	newElement->value = value;
	newElement->type = type;
	newElement->next = *symtab;
	*symtab = newElement;
}

static void *getElement(Symtab *symtab, const char *name, SymbolType type){
	for(Symtab *element = symtab; element != NULL; element = element->next){
		if(strcmp(element->name, name) == 0){
			if(element->type != type){
				fprintf(stderr, "Symbol \"%s\" not of type \"%s\". Exiting!\n", name, symbolTypeToString(type));
				exit(EXIT_FAILURE);
			}
			return element->value;
		}
	}
	fprintf(stderr, "Symbol \"%s\" undefined. Exiting!\n", name);
	exit(EXIT_FAILURE);
	return NULL;
}

const char *getStringSym(Symtab *symtab, const char *name){
	return (char *)getElement(symtab, name, SYMBOL_STRING);
}

Pipeline *getPipelineSym(Symtab *symtab, const char *name){
	return (Pipeline *)getElement(symtab, name, SYMBOL_PIPELINE);
}
