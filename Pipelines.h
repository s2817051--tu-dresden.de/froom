/*
 * Pipelines.h
 */
#ifndef __PIPELINES_H__
#define __PIPELINES_H__

#include <inttypes.h>
#include "map.h"

/**
 * @brief The pipeline structure
 */
typedef struct _Pipeline Pipeline;
/**
 * @brief The symbol table structure
 */
typedef struct _symtab Symtab;

/**
 * @brief The two supported types of symbols
 */
enum _SymbolType{
	SYMBOL_PIPELINE,
	SYMBOL_STRING
};

typedef enum _SymbolType SymbolType;

/**
 * @brief create an offset to be used in the referencetime param of createCSVMetricSource
 * @param timeString a string conforming to ISO 8601
 * @return an offset for relative timestamps
 */
uint64_t parseIsoTime(const char *timeString);
/**
 * @brief It creates a Source from a csv file
 * @param path the path of the file to read from
 * @param itemseparator the separator between items/lines
 * @param propertyseparator the separator between the properties of one item
 * @param timestamp the column containing the timestamp of the metric
 * @param resolution the resolution of the timestamp of the metric
 * @param referencetime the reference timestamp for timestamps of the metric (timestamps are relative to this)
 * @param selector a combination of (value, unit) pairs with value representing a column and unit being a descriptive string
 * @param useIsoTime whether the time column contains ISO datetime timestamps
 * @return a pipeline consisting of a csv source
 */
Pipeline *createCSVMetricSource(const char *path, const char *itemseparator, const char *propertyseparator, uint64_t time, uint64_t resolution, uint64_t referencetime, map *selector, bool useIsoTime);

/**
 * @brief It creates a Unifier
 * @param otherPipeline the other pipeline to unify with
 * @return a pipeline consisting of a unifier with the other pipeline already connected
 */
Pipeline *createUnifier(Pipeline *otherPipeline);

/**
 * @brief It creates a Sequencer
 * @param otherPipeline the other pipeline to process first
 * @return a pipeline consisting of a sequencer with the other pipeline already connected
 */
Pipeline *createSequencer(Pipeline *otherPipeline);

/**
 * @brief It creates a TimeSlicer
 * @param fromInSeconds the first timestamp (in seconds relative to start of trace) that should be retained
 * @param toInSeconds the last timestamp (in seconds relative to start of trace) that should be retained
 * @return a pipeline consisting of a TimeSlicer
 */
Pipeline *createTimeSlicer(double fromInSeconds, double toInSeconds);

/**
 * @brief It creates a Renamer
 * @param toBeReplaced a string to be replaced
 * @param replacement the replacement
 * @return a pipeline consisting of only a renamer
 */
Pipeline *createRenamer(const char *toBeReplaced, const char *replacement);

/**
 * @brief It creates an OTF2 source
 * @param path the path to read data from
 * @return a pipeline consisting of only a source
 */
Pipeline *createSource(const char *path);

/**
 * @brief It creates an OTF2 source with the same path as the other
 * @param pipeline a pipeline consisting of only an OTF2 source
 * @return a new pipeline consisting of only a source
 */
Pipeline *duplicateSource(Pipeline *pipeline);

/**
 * @brief It creates a ChromeTrace source
 * @param path the path to read data from
 * @return a pipeline consisting of only a source
 */
Pipeline *createChromeTraceSource(const char *path);

enum _ComparisonOperator{
	COMPARISON_EQUALITY,
	COMPARISON_LOWER_THAN,
	COMPARISON_LOWER_EQUAL,
	COMPARISON_GREATER_THAN,
	COMPARISON_GREATER_EQUAL
};

typedef enum _ComparisonOperator ComparisonOperator;

/**
 * @brief It creates a Location remover
 * @param comparisonOp the comparison operator
 * @param limit the value to compare with
 * @return a pipeline consisting of only a source
 */
Pipeline *createLocationRemover(ComparisonOperator comparisonOp, uint64_t limit);

/**
 * @brief It creates a region remover that also removes enter and leave events
 * @param pattern an extended regex that matches (partially) the region's name iff the region should be removed
 * @return a pipeline consisting of only a source
 */
Pipeline *createRegionRemover(const char *pattern);

/**
 * @brief It creates an MPI communication identifier collector
 * @return a pipeline consisting of only an MPI communication identifier collector
 */
Pipeline *createMPICommCollector(void);

/**
 * @brief It creates an adaptor that modifies identifiers used in MPI communication
 * @param mpiCommCollector must be an MPICommCollector
 * @return a pipeline consisting of only an MPI communication adaptor
 */
Pipeline *createMPICommAdaptor(Pipeline *mpiCommCollector);

/**
 * @brief It creates a sink
 * @param path the path to write data to
 * @return a pipeline consisting of only a sink
 */
Pipeline *createSink(const char *path);

/**
 * @brief It creates a pipeline by connecting other pipelines
 * @param pipeline1 the first part of the pipeline
 * @param pipeline2 the second part of the pipeline
 * @return a connected pipeline containing pipeline1 and pipeline2
 */
Pipeline *createPipeline(Pipeline *pipeline1, Pipeline *pipeline2);

/**
 * @brief It creates a sink at the end of pipeline and returns a new pipeline part with a source at the beginning, so that the source points to the trace archive the sink creates
 * @param pipeline the pipeline to be connected to a sink
 * @return a new pipeline part with a source at the beginning
 */
Pipeline *breakPipeline(Pipeline *pipeline);

/**
 * @brief store something in the symbol table
 * @param symtab the symbol table
 * @param name some symbol name
 * @param value some value to store
 * @param the type of the value
 */
void storeSym(Symtab **symtab, const char *name, void *value, SymbolType type);

/**
 * @brief retrieve a string from the symbol table
 * @param symtab the symbol table
 * @param name some symbol name
 * @return a string stored previously
 */
const char *getStringSym(Symtab *symtab, const char *name);

/**
 * @brief retrieve a pipeline from the symbol table
 * @param symtab the symbol table
 * @param name some symbol name
 * @return a pipeline stored previously
 */
Pipeline *getPipelineSym(Symtab *symtab, const char *name);

/**
 * @brief execute a pipeline
 * @param pipeline the pipeline to execute
 */
void executePipeline(Pipeline *pipeline);

/**
 * @brief release a pipeline when it is not needed anymore
 * @param pipeline the pipeline to release
 */
void releasePipeline(Pipeline *pipeline);
#endif /* __PIPELINES_H__ */
