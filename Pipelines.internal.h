#include <stdbool.h>

typedef void (*ReleaseCallback)(CallbackHolder *);

typedef struct _PipelineElem PipelineElem;
struct _PipelineElem{
	CallbackHolder *callbackHolder;
	ReleaseCallback releaseCallback;
	PipelineElem *next;
	PipelineElem *previous;
	PipelineElem *alternativePrevious;
};

typedef struct _Pipeline Pipeline;
struct _Pipeline{
	PipelineElem *first;
	PipelineElem *last;
};

Pipeline *asPipeline(CallbackHolder *callbackHolder,ReleaseCallback releaseCallback);
bool isProducedBySink(const char *filePath);
