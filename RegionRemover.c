/* Author: Jan Frenzel */
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "CallbackHolder.h"
#include "list.h"
#include "Pipelines.h"
#include "Pipelines.internal.h"
#include <sys/types.h>
#include <regex.h>

typedef struct _RegionRemover RegionRemover;
struct _RegionRemover{
	CallbackHolder callbackHolder;
	regex_t preg;
	list matchingStringList;
	list matchingRegionList;
};

static OTF2_CallbackCode RegionRemover_GlobalDef_String_cb( CallbackHolder *callbackHolder, OTF2_StringRef self, const char* string ){
	int regex_status;
#define REGEX_ERROR_BUF_SIZE 128
	OTF2_CallbackCode callbackCode = OTF2_CALLBACK_SUCCESS;
	RegionRemover *regionRemover = (RegionRemover *) callbackHolder;
	regex_status = regexec(&(regionRemover->preg), string, 0, NULL, 0);
	if(0 == regex_status){
		list_add(&(regionRemover->matchingStringList), AS_VOID_PTR(self));
		// Simplification: Pass on the empty string
		// TODO: actually drop the strings
		callbackCode = callbackHolder->next->onGlobalDef_String(callbackHolder->next, self, "");
	}else{
		callbackCode = callbackHolder->next->onGlobalDef_String(callbackHolder->next, self, string);
	}
	return callbackCode;
}

static OTF2_CallbackCode RegionRemover_GlobalDef_Region_cb( CallbackHolder* callbackHolder, OTF2_RegionRef self, OTF2_StringRef name, OTF2_StringRef canonicalName, OTF2_StringRef description, OTF2_RegionRole regionRole, OTF2_Paradigm paradigm, OTF2_RegionFlag regionFlags, OTF2_StringRef sourceFile, uint32_t beginLineNumber, uint32_t endLineNumber ){
	OTF2_CallbackCode callbackCode = OTF2_CALLBACK_SUCCESS;
	RegionRemover *regionRemover = (RegionRemover *) callbackHolder;
	uint32_t index = list_index(&(regionRemover->matchingStringList), AS_VOID_PTR(name), compareInt);
	if(UINT32_MAX == index){
		callbackCode = callbackHolder->next->onGlobalDef_Region(callbackHolder->next, self, name, canonicalName, description, regionRole, paradigm, regionFlags, sourceFile, beginLineNumber, endLineNumber);
	}else{
		list_add(&(regionRemover->matchingRegionList), AS_VOID_PTR(self));
		// Simplification: Pass on some invalid region
		// TODO: actually drop the region
		callbackCode = callbackHolder->next->onGlobalDef_Region(callbackHolder->next, self, 0, 0, 0, regionRole, paradigm, regionFlags, 0, -1, -1);
	}
	return callbackCode;
}

static OTF2_CallbackCode RegionRemover_Evt_Enter_cb( OTF2_LocationRef location, OTF2_TimeStamp time, uint64_t eventPosition, CallbackHolder* callbackHolder, OTF2_AttributeList *attributeList, OTF2_RegionRef region ){
	OTF2_CallbackCode callbackCode = OTF2_CALLBACK_SUCCESS;
	RegionRemover *regionRemover = (RegionRemover *) callbackHolder;
	uint32_t index = list_index(&(regionRemover->matchingRegionList), AS_VOID_PTR(region), compareInt);
	if(UINT32_MAX == index){
		callbackCode = callbackHolder->next->onEvt_Enter(location, time, eventPosition, callbackHolder->next, attributeList, region);
	}
	return callbackCode;
}

static OTF2_CallbackCode RegionRemover_Evt_Leave_cb( OTF2_LocationRef location, OTF2_TimeStamp time, uint64_t eventPosition, CallbackHolder* callbackHolder, OTF2_AttributeList *attributeList, OTF2_RegionRef region ){
	OTF2_CallbackCode callbackCode = OTF2_CALLBACK_SUCCESS;
	RegionRemover *regionRemover = (RegionRemover *) callbackHolder;
	uint32_t index = list_index(&(regionRemover->matchingRegionList), AS_VOID_PTR(region), compareInt);
	if(UINT32_MAX == index){
		callbackCode = callbackHolder->next->onEvt_Leave(location, time, eventPosition, callbackHolder->next, attributeList, region);
	}
	return callbackCode;
}

static void releaseRegionRemover(RegionRemover *regionRemover){
	list_delete(&(regionRemover->matchingStringList), del_list_element_func_nothing);
	list_delete(&(regionRemover->matchingRegionList), del_list_element_func_nothing);
	regfree(&(regionRemover->preg));
	free(regionRemover);
}

void transformIntoDefaultCallbackHolder(CallbackHolder *callbackHolder);

Pipeline *createRegionRemover(const char *regex_pattern){
	RegionRemover *regionRemover = (RegionRemover *)malloc(sizeof(RegionRemover));
	if(NULL != regionRemover){
		int regex_status;
		char regex_error_buf[REGEX_ERROR_BUF_SIZE];
#define REGEX_FLAGS (REG_EXTENDED | REG_NOSUB)
		if(0 != (regex_status = regcomp(&(regionRemover->preg), regex_pattern, REGEX_FLAGS))){
			regerror(regex_status, &(regionRemover->preg), regex_error_buf, REGEX_ERROR_BUF_SIZE);
			fprintf(stderr, "%s\n", regex_error_buf);
			regfree(&(regionRemover->preg));
			exit(EXIT_FAILURE);
		}
		transformIntoDefaultCallbackHolder(&(regionRemover->callbackHolder));
		regionRemover->callbackHolder.onGlobalDef_String = RegionRemover_GlobalDef_String_cb;
		regionRemover->callbackHolder.onGlobalDef_Region = RegionRemover_GlobalDef_Region_cb;
		regionRemover->callbackHolder.onEvt_Enter = RegionRemover_Evt_Enter_cb;
		regionRemover->callbackHolder.onEvt_Leave = RegionRemover_Evt_Leave_cb;
		list_initialize(&(regionRemover->matchingStringList));
		list_initialize(&(regionRemover->matchingRegionList));
	}
	return asPipeline((CallbackHolder *)regionRemover, (ReleaseCallback)releaseRegionRemover);
}


