#include <string.h>
#include "Pipelines.h"
#include "CallbackHolder.h"
#include "Pipelines.internal.h"

typedef struct _Renamer Renamer;
struct _Renamer{
	CallbackHolder callbackHolder;
	const char *toBeReplaced;
	const char *replacement;
};

static char *str_replace(const char *orig, const char *toBeReplaced, const char *replacement) {
	char *result; // the return string
	char *ins;    // the next insert point
	char *tmp;    // varies
	const char *countPos; // position for counting
	int len_toBeReplaced;  // length of toBeReplaced (the string to remove)
	int len_replacement; // length of replacement (the string to replace toBeReplaced with)
	int len_front; // distance between toBeReplaced and end of last toBeReplaced
	int count;    // number of replacements

	// sanity checks and initialization
	if (!orig || !toBeReplaced || !replacement){
		return NULL;
	}
	len_toBeReplaced = strlen(toBeReplaced);
	if (len_toBeReplaced == 0){
		return NULL; // empty toBeReplaced causes infinite loop during count
	}
	len_replacement = strlen(replacement);

	// count the number of replacements needed
	countPos = orig;
	for (count = 0; (tmp = strstr(countPos, toBeReplaced)); ++count) {
		countPos = tmp + len_toBeReplaced;
	}

	size_t new_count = strlen(orig) + (len_replacement - len_toBeReplaced) * count + 1;
	tmp = result = malloc(new_count);

	if (!result){
		return NULL;
	}

	memset(result, 0, new_count);
	// first time through the loop, all the variable are set correctly
	// from here on,
	//    tmp points to the end of the result string
	//    ins points to the next occurrence of toBeReplaced in orig
	//    orig points to the remainder of orig after "end of toBeReplaced"
	while (count--) {
		ins = strstr(orig, toBeReplaced);
		len_front = ins - orig;
		tmp = strncpy(tmp, orig, len_front) + len_front;
		tmp = strcpy(tmp, replacement) + len_replacement;
		orig += len_front + len_toBeReplaced; // move to next "end of toBeReplaced"
	}
	strcpy(tmp, orig);
	return result;
}

static OTF2_CallbackCode renameString_cb( CallbackHolder *callbackHolder, OTF2_StringRef self, const char* string ){
	char *replaced;
	Renamer *renamer = (Renamer *) callbackHolder;
	if( NULL == strstr(string, renamer->toBeReplaced) ){
		return callbackHolder->next->onGlobalDef_String(callbackHolder->next, self, string);
	}
	if(0 == strcmp(renamer->toBeReplaced, string) ){
		return callbackHolder->next->onGlobalDef_String(callbackHolder->next, self, renamer->replacement);
	}
	replaced = str_replace(string, renamer->toBeReplaced, renamer->replacement);
	if(NULL == replaced){
		return OTF2_CALLBACK_ERROR;
	}
	OTF2_CallbackCode callbackCode= callbackHolder->next->onGlobalDef_String(callbackHolder->next, self, replaced);
	free(replaced);
	return callbackCode;
}

static void releaseRenamer(Renamer *renamer){
	free((void *)(renamer->toBeReplaced));
	free((void *)(renamer->replacement));
	free(renamer);
}

void transformIntoDefaultCallbackHolder(CallbackHolder *callbackHolder);

Pipeline *createRenamer(const char *toBeReplaced, const char *replacement){
	Renamer *renamer = (Renamer *)malloc(sizeof(Renamer));
	if(NULL != renamer){
		transformIntoDefaultCallbackHolder(&(renamer->callbackHolder));
		renamer->callbackHolder.onGlobalDef_String = renameString_cb;
		renamer->toBeReplaced = strdup(toBeReplaced);
		renamer->replacement = strdup(replacement);
	}
	return asPipeline((CallbackHolder *)renamer, (ReleaseCallback)releaseRenamer);
}


