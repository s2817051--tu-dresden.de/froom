#include <stdlib.h>
#include "CallbackHolder.h"
#include "Pipelines.h"
#include "Pipelines.internal.h"
typedef struct _Sequencer Sequencer;
struct _Sequencer{
	CallbackHolder callbackHolder;
	bool isFirstSourceFinished;
};

static void sequencer_onDefFinished_cb(CallbackHolder *callbackHolder){
	Sequencer *sequencer = (Sequencer *)callbackHolder;
	if(sequencer->isFirstSourceFinished){
		CallbackHolder *nextCallbackHolder = callbackHolder->next;
		nextCallbackHolder->onDefFinished(nextCallbackHolder);
		sequencer->isFirstSourceFinished = false;
	}else{
		sequencer->isFirstSourceFinished = true;
	}
}

static void sequencer_onTimedDataFinished_cb(CallbackHolder *callbackHolder){
	Sequencer *sequencer = (Sequencer *)callbackHolder;
	if(sequencer->isFirstSourceFinished){
		CallbackHolder *nextCallbackHolder = callbackHolder->next;
		nextCallbackHolder->onTimedDataFinished(nextCallbackHolder);
		sequencer->isFirstSourceFinished = false;
	}else{
		sequencer->isFirstSourceFinished = true;
	}
}

void transformIntoDefaultCallbackHolder(CallbackHolder *callbackHolder);

Pipeline *createSequencer(Pipeline *otherBranch){
	Sequencer *sequencer = (Sequencer *)malloc(sizeof(Sequencer));
	transformIntoDefaultCallbackHolder(&(sequencer->callbackHolder));
	/*
	sequencer->callbackHolder.onDefFinished = sequencer_onDefFinished_cb;
	sequencer->callbackHolder.onTimedDataFinished = sequencer_onTimedDataFinished_cb;
	*/
	sequencer->isFirstSourceFinished = false;
	PipelineElem *pipelineElem = (PipelineElem *)malloc(sizeof(PipelineElem));
	pipelineElem->callbackHolder = &(sequencer->callbackHolder);
	pipelineElem->releaseCallback = (ReleaseCallback) free;
	pipelineElem->callbackHolder->next = NULL;
	pipelineElem->next = NULL;
	pipelineElem->previous = NULL;
	pipelineElem->alternativePrevious = otherBranch->last;
	otherBranch->last->next = pipelineElem;
	otherBranch->last->callbackHolder->next = &(sequencer->callbackHolder);
	Pipeline *pipeline = otherBranch;
	pipeline->first = pipelineElem;
	pipeline->last = pipelineElem;
	return pipeline;
}

