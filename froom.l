%{

/*
 * froom.l
 */

#include "list.h"
#include "Pipelines.h"
#include "froom_parser.h"

#include <stdio.h>

/* source: https://stackoverflow.com/questions/656703/how-does-flex-support-bison-location-exactly */
#define YY_USER_ACTION \
    yylloc->first_line = yylloc->last_line; \
    yylloc->first_column = yylloc->last_column; \
    for(int i = 0; yytext[i] != '\0'; i++) { \
        if(yytext[i] == '\n') { \
            yylloc->last_line++; \
            yylloc->last_column = 0; \
        } \
        else { \
            yylloc->last_column++; \
        } \
    }

%}

%option outfile="froom_lexer.c" header-file="froom_lexer.h"
%option warn nodefault

%option reentrant noyywrap never-interactive nounistd nounput noinput
%option bison-bridge
%option bison-locations

%%

"/*"([^\*]|(\*)*[^\*/])*(\*)*"*/" { continue; /* Skip C-style comment */}
"//"[^\n]*\n                      { continue; /* Skip C++-style comment. */ }
[ \r\n\t]*                        { continue; /* Skip blanks. */ }
\"(\\.|[^\\"])*\"                 { yylval->stringvalue = strndup(yytext+1,strlen(yytext)-2); return TOKEN_STRING;}
"Renamer"                         { return TOKEN_RENAMER; }
"Unifier"                         { return TOKEN_UNIFIER; }
"OTF2Source"                      { return TOKEN_OTF2SOURCE; }
"OTF2Sink"                        { return TOKEN_OTF2SINK; }
"TimeSlicer"                      { return TOKEN_TIMESLICER; }
"from"                            { return TOKEN_FROM; }
"to"                              { return TOKEN_TO; }
"CSVMetricSource"                 { return TOKEN_CSVMETRICSOURCE; }
"Sequencer"                       { return TOKEN_SEQUENCER; }
"itemseparator"                   { return TOKEN_ITEMSEP; }
"propertyseparator"               { return TOKEN_PROPERTYSEP; }
"Column"                          { return TOKEN_COLUMN; }
"time"                            { return TOKEN_TIME; }
"isotime"                         { return TOKEN_ISOTIME; }
"referencetime"                   { return TOKEN_REFERENCETIME; }
"tickspersecond"                  { return TOKEN_TICKS_PER_SECOND; }
"value"                           { return TOKEN_VALUE; }
"unit"                            { return TOKEN_UNIT; }
"metrics"                         { return TOKEN_METRICS; }
"->"                              { return TOKEN_CONNECT; }
"("                               { return TOKEN_LPAREN; }
")"                               { return TOKEN_RPAREN; }
"="                               { return TOKEN_EQUAL; }
","                               { return TOKEN_COMMA; }
";"                               { return TOKEN_SEMICOLON; }
"ChromeTraceSource"               { return TOKEN_CHROMETRACESOURCE; }
"LocationRemover"                 { return TOKEN_LOCATIONREMOVER; }
"eventCount"                      { return TOKEN_EVENTCOUNT; }
"MPICommCollector"                { return TOKEN_MPICOMMCOLLECTOR; }
"MPICommAdaptor"                  { return TOKEN_MPICOMMADAPTOR; }
"mpiComms"                        { return TOKEN_MPICOMMLIST; }
"<"                               { return TOKEN_LOWERTHAN; }
">"                               { return TOKEN_GREATERTHAN; }
"<="                              { return TOKEN_LOWEROREQUAL; }
">="                              { return TOKEN_GREATEROREQUAL; }
"=="                              { return TOKEN_EQUALITY; }
"RegionRemover"                   { return TOKEN_REGIONREMOVER; }
[A-Za-z_][A-Za-z0-9_]*            { yylval->id = strdup(yytext); return TOKEN_ID;}
[0-9]+[.][0-9]*                   { sscanf(yytext, "%lf", &yylval->doublevalue); return TOKEN_DOUBLE;}
[0-9]+                            { sscanf(yytext, "%"SCNu64, &yylval->number); return TOKEN_NUMBER;}

.                                 { fprintf(stderr, "Unrecognized character: %c on line %d, column %d\n", *yytext, yylloc->last_line, yylloc->last_column); exit(EXIT_FAILURE);}

%%

int yyerror(YYLTYPE *locp, list **pipelineList, Symtab **symtab, yyscan_t scanner, const char *msg) {
    fprintf(stderr, "Error at line %d, column %d: %s\n", locp->first_line, locp->first_column, msg);
    return 0;
}
