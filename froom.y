%{

/*
 * froom.y
 */

#include "utils.h"
#include "list.h"
#include "Pipelines.h"
#include "froom_parser.h"
#include "froom_lexer.h"

// reference the implementation provided in Lexer.l
int yyerror(YYLTYPE *locp, list *pipelineList, Symtab **symtab, yyscan_t scanner, const char *msg);

%}

%code requires {
#include "list.h"
typedef void* yyscan_t;
}

%output  "froom_parser.c"
%defines "froom_parser.h"

%locations
%define parse.error verbose
%define api.pure
%lex-param   { yyscan_t scanner }
%parse-param { list *pipelineList }
%parse-param { Symtab **symtab }
%parse-param { yyscan_t scanner }

%union {
    uint64_t number;
    double doublevalue;
    const char *id;
    const char *stringvalue;
    Pipeline *pipeline;
    map *mappings;
}

%define api.token.prefix {TOKEN_}
%token LPAREN   "("
%token RPAREN   ")"
%token CONNECT  "->"
%token EQUAL    "="
%token EQUALITY    "=="
%token LOWERTHAN    "<"
%token LOWEROREQUAL    "<="
%token GREATERTHAN    ">"
%token GREATEROREQUAL    ">="
%token COMMA    ","
%token SEMICOLON    ";"
%token OTF2SOURCE  "OTF2Source"
%token OTF2SINK  "OTF2Sink"
%token RENAMER  "Renamer"
%token UNIFIER  "Unifier"
%token TIMESLICER  "TimeSlicer"
%token FROM  "from"
%token TO  "to"
%token CSVMETRICSOURCE  "CSVMetricSource"
%token LOCATIONREMOVER  "LocationRemover"
%token EVENTCOUNT  "eventCount"
%token SEQUENCER  "Sequencer"
%token ITEMSEP  "itemseparator"
%token PROPERTYSEP  "propertyseparator"
%token COLUMN  "Column"
%token TIME  "time"
%token REFERENCETIME  "referencetime"
%token TICKS_PER_SECOND  "tickspersecond"
%token ISOTIME  "isotime"
%token METRICS  "metrics"
%token VALUE  "value"
%token UNIT  "unit"
%token CHROMETRACESOURCE "ChromeTraceSource"
%token MPICOMMCOLLECTOR "MPICommCollector"
%token MPICOMMADAPTOR "MPICommAdaptor"
%token MPICOMMLIST "mpiComms"
%token REGIONREMOVER "RegionRemover"
%token <stringvalue> STRING   "string"
%token <id> ID   "variable"
%token <number> NUMBER "number"
%token <doublevalue> DOUBLE "double"

%type <pipeline> pipeline
%type <pipeline> source
%type <pipeline> operatorchain
%type <pipeline> operator
%type <pipeline> unifierargs
%type <pipeline> sink
%type <pipeline> csvmetricsource
%type <pipeline> sequencerargs
%type <number> columnref
%type <doublevalue> doubleorvalue
%type <stringvalue> stringorvariable
%type <mappings> csvmetricselectors
%type <mappings> csvmetricselector

%left "->"

%printer { fprintf (yyo, "%lf", $$); } <doublevalue>;
%printer { fprintf (yyo, "%"PRIu64, $$); } <number>;
%printer { fprintf (yyo, "%s", $$); } <id>;
%printer { fprintf (yyo, "%s", $$); } <stringvalue>;
%%

pipelines
    : pipelines pipeline ";" { if( NULL != $2 ) list_add(pipelineList, $2); }
    | pipeline ";" { if( NULL != $1 ) list_add(pipelineList, $1); }
    ;

pipeline
    : operatorchain "->" sink { $$ = createPipeline( $1, $3); }
    | operatorchain "->" "variable" { $$ = NULL; storeSym( symtab, $3, $1, SYMBOL_PIPELINE ); }
    ;

operatorchain
    : source { $$ = $1; }
    | operatorchain "->" "MPICommAdaptor" { list_add(pipelineList, $1); Pipeline *newSource = breakPipeline( $1 ); Pipeline *duplicatedSource = duplicateSource(newSource); Pipeline *commCollector = createPipeline(newSource, createMPICommCollector()); $$ = createPipeline(duplicatedSource, createMPICommAdaptor(commCollector)); }
    | operatorchain "->" operator { $$ = createPipeline( $1, $3 ); }
    ;

operator
    : "Renamer" "(" stringorvariable "->" stringorvariable ")" { $$ = createRenamer( $3, $5 ); }
    | "Unifier" "(" unifierargs ")" { $$ = $3; }
    | "TimeSlicer" "(" "from" "=" doubleorvalue "," "to" "=" doubleorvalue ")" { $$ = createTimeSlicer( $5, $9 ); }
    | "LocationRemover" "(" "eventCount" "==" "number" ")" { $$ = createLocationRemover( COMPARISON_EQUALITY, $5 ); }
    | "LocationRemover" "(" "eventCount" "<" "number" ")" { $$ = createLocationRemover( COMPARISON_LOWER_THAN, $5 ); }
    | "LocationRemover" "(" "eventCount" "<=" "number" ")" { $$ = createLocationRemover( COMPARISON_LOWER_EQUAL, $5 ); }
    | "LocationRemover" "(" "eventCount" ">" "number" ")" { $$ = createLocationRemover( COMPARISON_GREATER_THAN, $5 ); }
    | "LocationRemover" "(" "eventCount" ">=" "number" ")" { $$ = createLocationRemover( COMPARISON_GREATER_EQUAL, $5 ); }
    | "Sequencer" "(" sequencerargs ")" { $$ = $3; }
    | "RegionRemover" "(" stringorvariable ")" { $$ = createRegionRemover( $3 ); }
    ;

sequencerargs
    : "variable" { $$ = createSequencer( getPipelineSym( *symtab, $1 ) ); }
    | sequencerargs "," "variable" { $$ = createPipeline($1, createSequencer( getPipelineSym( *symtab, $3 ) ) ); }
    ;

doubleorvalue
    : "double" { $$ = $1; }
    | "number" { $$ = (double) $1; }
    ;

stringorvariable
    : "string" { $$ = $1; }
    | "variable" { $$ = getStringSym( *symtab, $1 ); }
    ;

unifierargs
    : "variable" { $$ = createUnifier( getPipelineSym( *symtab, $1 ) ); }
    | unifierargs "," "variable" { $$ = createPipeline($1, createUnifier( getPipelineSym( *symtab, $3 ) ) ); }
    ;

source
    : "OTF2Source" "(" stringorvariable ")" { $$ = createSource( $3 ); }
    | csvmetricsource { $$ = $1; }
    | "variable" { $$ = getPipelineSym( *symtab, $1 ); }
    | "ChromeTraceSource" "(" stringorvariable ")" { $$ = createChromeTraceSource( $3 ); }
    ;

csvmetricsource
    : "CSVMetricSource" "(" stringorvariable[PATH] "," "itemseparator" "=" stringorvariable[ITEMSEP] "," "propertyseparator" "=" stringorvariable[PROPERTYSEP] "," "time" "=" columnref[TIMEREF] "," "tickspersecond" "=" "number"[RESOLUTION] "," "metrics" "=" csvmetricselectors[SELECTOR] ")" { $$ = createCSVMetricSource( $PATH, $ITEMSEP, $PROPERTYSEP, $TIMEREF, $RESOLUTION, 0, $SELECTOR, false ); }
    | "CSVMetricSource" "(" stringorvariable[PATH] "," "itemseparator" "=" stringorvariable[ITEMSEP] "," "propertyseparator" "=" stringorvariable[PROPERTYSEP] "," "time" "=" columnref[TIMEREF] "," "tickspersecond" "=" "number"[RESOLUTION] "," "referencetime" "=" stringorvariable[REFERENCETIME] "," "metrics" "=" csvmetricselectors[SELECTOR] ")" { $$ = createCSVMetricSource( $PATH, $ITEMSEP, $PROPERTYSEP, $TIMEREF, $RESOLUTION, parseIsoTime($REFERENCETIME), $SELECTOR, false ); }
    | "CSVMetricSource" "(" stringorvariable[PATH] "," "itemseparator" "=" stringorvariable[ITEMSEP] "," "propertyseparator" "=" stringorvariable[PROPERTYSEP] "," "isotime" "=" columnref[TIMEREF] "," "metrics" "=" csvmetricselectors[SELECTOR] ")" { $$ = createCSVMetricSource( $PATH, $ITEMSEP, $PROPERTYSEP, $TIMEREF, 0, 0, $SELECTOR, true ); }
    ;

csvmetricselectors
    : csvmetricselectors "," csvmetricselector { map * aMap = $1; map_add(aMap, $3->buckets[0].entries[0].from, $3->buckets[0].entries[0].to); map_delete($3, del_func_nothing); $$ = $1; }
    | csvmetricselector { $$ = $1; }
    ;

csvmetricselector
    : "(" "value" "=" columnref[VALUEREF] "," "unit" "=" stringorvariable[UNIT] ")" { map *newMap = (map *)malloc(sizeof(map)); map_initialize(newMap, compareColumnIndexesFromMap, hashToConstant); map_add(newMap, AS_VOID_PTR($VALUEREF), $UNIT); $$ = newMap;}
    ;

columnref
    : "Column" "(" "number" ")" { $$ = $3; }
    ;

sink
    : "OTF2Sink" "(" stringorvariable ")" { $$ = createSink( $3 ); }
    ;

%%
