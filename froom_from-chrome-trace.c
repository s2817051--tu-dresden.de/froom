#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Pipelines.h"
#include "utils.h"

void usage(void){
	puts(
"froom-from-chrome-trace <source_path> <sink_path>\n"
"\t<source_path>  path to a chrome trace file\n"
"\t<sink_path>    name of a directory to create with updated trace data\n\n"
"Note: <sink_path> must not exist.\n"
	);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv){
	if(argc != 3){
		usage();
	}
	if(0 == strcmp("--help", argv[1]) || 0 == strcmp("help", argv[1])){
		usage();
	}
	char *source_path = argv[1];
	char *sink_path = argv[2];
	Pipeline *sink = createSink(sink_path);
	Pipeline *source = createChromeTraceSource(source_path);
	Pipeline *p = createPipeline(source, sink);
	executePipeline(p);
	releasePipeline(p);
	return 0;
}
