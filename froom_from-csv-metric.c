#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Pipelines.h"
#include "utils.h"

void usage(void){
	puts(
"froom-from-csv-metric <source_path> <itemsep> <propertysep> <timecol> <tickspersec> <reference> <metriccol1> <metricunit1> ... <sink_path>\n"
"\t<source_path>  path to a csv file\n"
"\t<itemsep>      item separator character (e.g. \'\n\')\n"
"\t<propertysep>  property separator character (e.g. \',\')\n"
"\t<timecol>      index of the column containing timestamps\n"
"\t<tickspersec>  time resolution/ticks per second (use \"iso\" if column is in iso time)\n"
"\t<reference>    reference time stamp\n"
"\t<metriccol1>   metric column specification: column index\n"
"\t<metricunit>   metric column specification: unit\n"
"\t...            metric column specification (in pairs of column index and unit as with first metric)\n"
"\t<sink_path>    name of a directory to create with updated trace data\n\n"
"Note: <sink_path> must not exist.\n"
"      <metriccol> and <metricunit> must be specified in pairs.\n"
	);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv){
	if(argc < 10 || 1 == (argc & 1)){
		usage();
	}
	if(0 == strcmp("--help", argv[1]) || 0 == strcmp("help", argv[1])){
		usage();
	}
	char *source_path = argv[1];
	char *itemsep = argv[2];
	char *propertysep = argv[3];
	char *timecol_string = argv[4];
	char *tickspersec = argv[5];
	char *reference = argv[6];
	char *sink_path = argv[argc - 1];
	Pipeline *sink = createSink(sink_path);
	uint64_t timecol;
	sscanf(timecol_string, "%"SCNu64, &timecol);
	bool isotime = false;
	uint64_t resolution = 0;
	uint64_t referenceTime = 0;
	if(0 == strcmp("iso", tickspersec)){
		isotime = true;
	}else{
		isotime = false;
		sscanf(tickspersec, "%"SCNu64, &resolution);
		referenceTime = parseIsoTime(reference);
	}
	
	map columnspecs;
	map_initialize(&columnspecs, compareColumnIndexesFromMap, hashToConstant);
	for(int i = 7; i < argc - 1; i += 2){
		char *col_string = argv[i];
		char *unit = argv[i + 1];
		uint64_t col;
		sscanf(col_string, "%"SCNu64, &col);
		map_add(&columnspecs, AS_VOID_PTR(col), unit);
	}
	Pipeline *source = createCSVMetricSource(source_path, itemsep, propertysep, timecol, resolution, referenceTime, &columnspecs, isotime);
	map_delete(&columnspecs, del_func_nothing);
	Pipeline *p = createPipeline(source, sink);
	executePipeline(p);
	releasePipeline(p);
	return 0;
}
