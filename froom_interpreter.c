/*
 * froom_interpreter.c
 */

#include "Pipelines.h"
#include "froom_parser.h"
#include "froom_lexer.h"
#include "list.h"

#include <stdio.h>

int readPipelineListFromFile(list *pipelineList, Symtab **symtab, FILE *fileToParse)
{
	yyscan_t scanner;

	if (yylex_init(&scanner)) {
		/* could not initialize */
		return -1;
	}

	yyset_in(fileToParse, scanner);

	if (yyparse(pipelineList, symtab, scanner)) {
		/* error parsing */
		return -1;
	}


	yylex_destroy(scanner);

	return 0;
}

int fillPipelineListFromFile(list *pipelineList, Symtab **symtab, const char *filename)
{
	FILE *myfile = fopen(filename, "r");
	int result = readPipelineListFromFile(pipelineList, symtab, myfile);
	fclose(myfile);
	return result;
}

int main(int argc, char **argv)
{
//	yydebug = 1;
	Symtab *symtab = NULL;
	if(argc < 2){
		fputs("Error: input filename required\n", stderr);
		exit(1);
	}
	for(int currentArg=2; currentArg<argc; currentArg++){
		char *argument = argv[currentArg];
		char *name = strsep(&argument, "=");
		char *value = argument;
		storeSym(&symtab, name, value, SYMBOL_STRING);
	}
	list pipelineList;
	list_initialize(&pipelineList);
	int result = fillPipelineListFromFile(&pipelineList, &symtab, argv[1]);
	if(0 == result){
		for(int i = 0; i < pipelineList.entryCount; i++){
			Pipeline *e = pipelineList.entries[i];
			executePipeline(e);
		}
		list_delete(&pipelineList, (list_element_data_func) releasePipeline);
	}else{
		fputs("Please correct syntax errors!\n", stderr);
	}
	return 0;
}
