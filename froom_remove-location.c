#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Pipelines.h"

void usage(void){
	puts(
"froom-remove-location <source_path> <comparison> <number> <sink_path>\n"
"\t<source_path>  path to an anchor file of a trace archive\n"
"\t<comparison>   one of equal, lower, lower_equal, greater, greater_equal\n"
"\t<number>       number of events\n"
"\t<sink_path>    name of a directory to create with updated trace data\n\n"
"Note: <sink_path> must not exist.\n"
	);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv){
	if(argc != 5){
		usage();
	}
	if(0 == strcmp("--help", argv[1]) || 0 == strcmp("help", argv[1])){
		usage();
	}
	char *source_path = argv[1];
	char *comparison = argv[2];
	char *number_string = argv[3];
	char *sink_path = argv[4];
	ComparisonOperator op;
	uint64_t number;
	sscanf(number_string, "%"SCNu64, &number);
	if(0 == strcmp("lower", comparison)){
		op = COMPARISON_LOWER_THAN;
	}else if(0 == strcmp("lower_equal", comparison)){
		op = COMPARISON_LOWER_EQUAL;
	}else if(0 == strcmp("greater", comparison)){
		op = COMPARISON_GREATER_THAN;
	}else if(0 == strcmp("greater_equal", comparison)){
		op = COMPARISON_GREATER_EQUAL;
	}else if(0 == strcmp("equal", comparison)){
		op = COMPARISON_EQUALITY;
	}else{
		fputs("Unknown comparison operator!\n", stderr);
		exit(EXIT_FAILURE);
	}
	Pipeline *sink = createSink(sink_path);
	Pipeline *locationRemover = createLocationRemover(op, number);
	Pipeline *source = createSource(source_path);
	Pipeline *p = createPipeline(source, createPipeline( locationRemover, sink) );
	executePipeline(p);
	releasePipeline(p);
	return 0;
}
