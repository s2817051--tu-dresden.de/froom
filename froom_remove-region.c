#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Pipelines.h"

void usage(void){
	puts(
"froom-remove-region <source_path> <name> <sink_path>\n"
"\t<source_path>  path to an anchor file of a trace archive\n"
"\t<name>         name of a region to be removed, as a regular expression\n"
"\t<sink_path>    name of a directory to create with updated trace data\n\n"
"Note: <sink_path> must not exist.\n"
	);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv){
	if(argc != 4){
		usage();
	}
	if(0 == strcmp("--help", argv[1]) || 0 == strcmp("help", argv[1])){
		usage();
	}
	char *source_path = argv[1];
	char *name = argv[2];
	char *sink_path = argv[3];
	Pipeline *sink = createSink(sink_path);
	Pipeline *regionRemover = createRegionRemover(name);
	Pipeline *source = createSource(source_path);
	Pipeline *p = createPipeline(source, createPipeline( regionRemover, sink) );
	executePipeline(p);
	releasePipeline(p);
	return 0;
}
