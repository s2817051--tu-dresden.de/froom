#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Pipelines.h"

void usage(void){
	puts(
"froom-rename <source_path> <to_replace> <replacement> <sink_path>\n"
"\t<source_path>  path to an anchor file of a trace archive\n"
"\t<to_replace>   extended regular expression describing string to replace\n"
"\t<replacement>  the text that replaces the found occurences\n"
"\t<sink_path>    name of a directory to create with updated trace data\n\n"
"Note: <sink_path> must not exist.\n"
	);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv){
	if(argc != 5){
		usage();
	}
	if(0 == strcmp("--help", argv[1]) || 0 == strcmp("help", argv[1])){
		usage();
	}
	char *source_path = argv[1];
	char *to_replace = argv[2];
	char *replacement = argv[3];
	char *sink_path = argv[4];
	Pipeline *sink = createSink(sink_path);
	Pipeline *renamer = createRenamer(to_replace, replacement);
	Pipeline *source = createSource(source_path);
	Pipeline *p = createPipeline(source, createPipeline( renamer, sink) );
	executePipeline(p);
	releasePipeline(p);
	return 0;
}
