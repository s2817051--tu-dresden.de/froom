#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Pipelines.h"

void usage(void){
	puts(
"froom-slice <source_path> <from> <to> <sink_path>\n"
"\t<source_path>  path to an anchor file of a trace archive\n"
"\t<from>         first timestamp that should appear in sink\n"
"\t<to>           last timestamp that should appear in sink\n"
"\t<sink_path>    name of a directory to create with updated trace data\n\n"
"Note: <sink_path> must not exist.\n"
	);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv){
	if(argc != 5){
		usage();
	}
	if(0 == strcmp("--help", argv[1]) || 0 == strcmp("help", argv[1])){
		usage();
	}
	char *source_path = argv[1];
	char *first_string = argv[2];
	char *last_string = argv[3];
	char *sink_path = argv[4];
	double first;
	double last;
	sscanf(first_string, "%lf", &first);
	sscanf(last_string, "%lf", &last);
	Pipeline *sink = createSink(sink_path);
	Pipeline *timeslicer = createTimeSlicer(first, last);
	Pipeline *source = createSource(source_path);
	Pipeline *p = createPipeline(source, createPipeline( timeslicer, sink) );
	executePipeline(p);
	releasePipeline(p);
	return 0;
}
