#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Pipelines.h"

void usage(void){
	puts(
"froom-unify <source1_path> <source2_path> ... <sink_path>\n"
"\t<source1_path>  path to an anchor file of a trace archive\n"
"\t<source2_path>  path to another anchor file of a trace archive\n"
"\t...             a potential empty list of further pathes to other anchor files\n"
"\t<sink_path>    name of a directory to create with updated trace data\n\n"
"Note: <sink_path> must not exist.\n"
	);
	exit(EXIT_FAILURE);
}

int main(int argc, char **argv){
	if(argc < 4){
		usage();
	}
	if(0 == strcmp("--help", argv[1]) || 0 == strcmp("help", argv[1])){
		usage();
	}
	char *source1_path = argv[1];
	char *sink_path = argv[argc - 1];
	Pipeline *source = createSource(source1_path);
	Pipeline *sink = createSink(sink_path);
	Pipeline *p = source;
	for(int i = 2; i < argc - 1; i++){
		char *next_source_path = argv[i];
		Pipeline *next_source = createSource(next_source_path);
		p = createPipeline(p, createUnifier(next_source));
	}
	p = createPipeline(p, sink);
	executePipeline(p);
	releasePipeline(p);
	return 0;
}
