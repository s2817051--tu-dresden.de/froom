{{@ set definitionList = ["Region", "Group", "SystemTreeNode", "LocationGroup", "Location", "Paradigm", "Comm", "Attribute", "Callpath", "Parameter", "MetricMember", "MetricClass", "IoParadigm", "IoRegularFile", "IoHandle", "CartTopology", "CartCoordinate", "RmaWin"] -@}}
{{@ set nocompareList = ["Group", "LocationGroup", "Location", "Paradigm", "Comm", "MetricClass", "IoParadigm", "CartTopology", "CartCoordinate"] -@}}
{{@- for definition in defs|global_defs: -@}}
{{@- if definition.name in definitionList -@}}
typedef struct _My@@definition.name@@ My@@definition.name@@;
struct _My@@definition.name@@{
{{@- for attribute in definition.attributes: @}}
{{@- if attribute.name != "self" @}}
	@@attribute.type@@ @@attribute.name-@@;
{{@- if attribute.array_attributes is defined -@}}
{{@- for aattribute in attribute.array_attributes: @}}
	const @@aattribute.type@@* @@aattribute.name@@;
{{@- endfor -@}}
{{@- endif -@}}
{{@- endif -@}}
{{@- endfor @}}
};

My@@definition.name@@ *My@@definition.name@@_Create(void){
	My@@definition.name@@ *tmp=(My@@definition.name@@ *)malloc(sizeof(My@@definition.name@@));
	return tmp;
}

My@@definition.name@@ *My@@definition.name@@_Dup(My@@definition.name@@ *to_duplicate){
	My@@definition.name@@ *duplicated=My@@definition.name@@_Create();
	memcpy(duplicated, to_duplicate, sizeof(My@@definition.name@@));
	return duplicated;
}

void My@@definition.name@@_SetValues(My@@definition.name@@ *thing
{{@- for attribute in definition.attributes: -@}}
{{@- if attribute.name != "self" -@}}
, @@-attribute.type@@ @@attribute.name-@@
{{@- if attribute.array_attributes is defined -@}}
{{@- for aattribute in attribute.array_attributes: -@}}
, const @@aattribute.type@@* @@aattribute.name@@
{{@- endfor -@}}
{{@- endif -@}}
{{@- endif -@}}
{{@- endfor -@}}
 ){
	if(thing != NULL){
{{@- for attribute in definition.attributes: @}}
{{@- if attribute.name != "self" @}}
		thing->@@attribute.name@@ = @@attribute.name@@;
{{@- if attribute.array_attributes is defined -@}}
{{@- for aattribute in attribute.array_attributes: @}}
		thing->@@aattribute.name@@ = @@aattribute.name@@;
{{@ endfor -@}}
{{@- endif -@}}
{{@- endif -@}}
{{@- endfor @}}
	}
}

{{@ if definition.name not in nocompareList -@}}
int compare@@definition.name@@(const void *firstPtr, const void *secondPtr){
	const My@@definition.name@@ *first=(const My@@definition.name@@ *) firstPtr;
	const My@@definition.name@@ *second=(const My@@definition.name@@ *) secondPtr;
{{@- for attribute in definition.attributes: @}}
{{@- if attribute.name != "self" @}}
	if(first->@@attribute.name@@ < second->@@attribute.name@@) {
		return -1;
	}else if(first->@@attribute.name@@ > second->@@attribute.name@@){
		return 1;
	}
{{@- if attribute.array_attributes is defined -@}}
{{@- for aattribute in attribute.array_attributes: @}}
	int i;
	for(i = 0; i < first->@@attribute.name@@; i++){
		if(first->@@aattribute.name@@[i] < second->@@aattribute.name@@[i]) {
			return -1;
		}else if(first->@@aattribute.name@@[i] > second->@@aattribute.name@@[i]) {
			return 1;
		}
	}
{{@ endfor -@}}
{{@- endif -@}}
{{@- endif -@}}
{{@- endfor @}}
	return 0;
}

int compare@@definition.name@@MapFrom(const void *firstPtr, const void *secondPtr){
	const mapentry *firstEntry = (const mapentry *) firstPtr;
	const mapentry *secondEntry = (const mapentry *) secondPtr;
	return compare@@definition.name@@(firstEntry->from, secondEntry->from);
}

{{@ endif -@}}
{{@- endif -@}}
{{@- endfor -@}}

int compareMetricClass(const void *firstPtr, const void *secondPtr){
	const MyMetricClass *first=(const MyMetricClass *) firstPtr;
	const MyMetricClass *second=(const MyMetricClass *) secondPtr;
	if(first->numberOfMetrics < second->numberOfMetrics) {
		return -1;
	}else if(first->numberOfMetrics > second->numberOfMetrics){
		return 1;
	}
	if(first->metricOccurrence < second->metricOccurrence) {
		return -1;
	}else if(first->metricOccurrence > second->metricOccurrence){
		return 1;
	}
	if(first->recorderKind < second->recorderKind) {
		return -1;
	}else if(first->recorderKind > second->recorderKind){
		return 1;
	}
	int i;
	for(i = 0; i < first->numberOfMetrics; i++){
		if(first->metricMembers[i] < second->metricMembers[i]) {
			return -1;
		}else if(first->metricMembers[i] > second->metricMembers[i]){
			return 1;
		}
	}
	return 0;
}

int compareMetricClassMapFrom(const void *firstPtr, const void *secondPtr){
	const mapentry *firstEntry = (const mapentry *) firstPtr;
	const mapentry *secondEntry = (const mapentry *) secondPtr;
	return compareMetricClass(firstEntry->from, secondEntry->from);
}

/*
#define MetricInstanceFunc(x) \
	FIELD(OTF2_MetricRef, metricClass, x) \
	FIELD(OTF2_LocationRef, recorder, x) \
	FIELD(OTF2_MetricScope, metricScope, x) \
	FIELD(uint64_t, scope, x)
DECL_OBJ_WITH_COMPARE_FUNC(MetricInstance,MetricInstanceFunc)
*/

int compareIoParadigm(const void *firstPtr, const void *secondPtr){
	const MyIoParadigm *first=(const MyIoParadigm *) firstPtr;
	const MyIoParadigm *second=(const MyIoParadigm *) secondPtr;
	if(first->identification < second->identification) {
		return -1;
	}else if(first->identification > second->identification){
		return 1;
	}
	if(first->name < second->name) {
		return -1;
	}else if(first->name > second->name){
		return 1;
	}
	if(first->ioParadigmClass < second->ioParadigmClass) {
		return -1;
	}else if(first->ioParadigmClass > second->ioParadigmClass){
		return 1;
	}
	if(first->ioParadigmFlags < second->ioParadigmFlags) {
		return -1;
	}else if(first->ioParadigmFlags > second->ioParadigmFlags){
		return 1;
	}
	if(first->numberOfProperties < second->numberOfProperties) {
		return -1;
	}else if(first->numberOfProperties > second->numberOfProperties){
		return 1;
	}
	int i;
	for(i = 0; i < first->numberOfProperties; i++){
		if(first->properties[i] < second->properties[i]) {
			return -1;
		}else if(first->properties[i] > second->properties[i]){
			return 1;
		}
		if(first->types[i] < second->types[i]) {
			return -1;
		}else if(first->types[i] > second->types[i]){
			return 1;
		}
		if(first->values[i].uint64 < second->values[i].uint64) {
			return -1;
		}else if(first->values[i].uint64 > second->values[i].uint64){
			return 1;
		}
	}
	return 0;
}

int compareIoParadigmMapFrom(const void *firstPtr, const void *secondPtr){
	const mapentry *firstEntry = (const mapentry *) firstPtr;
	const mapentry *secondEntry = (const mapentry *) secondPtr;
	return compareIoParadigm(firstEntry->from, secondEntry->from);
}

