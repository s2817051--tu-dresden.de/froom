/* This file was GENERATED by Jan Frenzel - DO NOT MODIFY */
/* Author: Jan Frenzel */
#include "utils.h"
#include "CallbackHolder.h"
#include "list.h"
#include "Pipelines.h"
#include "Pipelines.internal.h"

typedef struct _LocationState LocationState;
struct _LocationState{
	list regionStackBeforeSlice;
	list regionStackInSlice;
	OTF2_RegionRef lastEnteredRegion;
	bool writingLeaveRequired;
};

typedef struct _TimeSlicer TimeSlicer;
struct _TimeSlicer{
	CallbackHolder callbackHolder;
	double lowerBoundInSeconds;
	double upperBoundInSeconds;
	OTF2_TimeStamp lowerBound;
	OTF2_TimeStamp upperBound;
	map locationStates; /* location -> locationState */
};

{{@ for definitiontype,fil,timeparam,addarg,addparam in [("Evt",events,"time",", uint64_t eventPosition",",eventPosition"),("Snap",snaps,"snapTime","","")] @}}
static OTF2_CallbackCode @@definitiontype@@_Unknown_TimeSlice_cb( OTF2_LocationRef location, OTF2_TimeStamp @@timeparam@@@@addarg@@, CallbackHolder *callbackHolder, OTF2_AttributeList* attributeList ){
	TimeSlicer *timeSlicer = (TimeSlicer *)callbackHolder;
	if((timeSlicer->lowerBound < @@timeparam@@) && (@@timeparam@@ < timeSlicer->upperBound)){
		return callbackHolder->next->on@@definitiontype@@_Unknown(location,@@timeparam@@@@addparam@@,callbackHolder->next,attributeList);
	}
	return OTF2_CALLBACK_SUCCESS;
}
//TODO: handle IO begin/end, message send/receive, rma collective begin/end, thread team begin/end, program begin/end
{{@ for definition in fil @}}
static OTF2_CallbackCode @@definitiontype@@_@@definition.name@@_TimeSlice_cb( OTF2_LocationRef location, OTF2_TimeStamp @@timeparam@@@@addarg@@, CallbackHolder *callbackHolder, OTF2_AttributeList* attributeList
{{@- for attribute in definition.attributes: -@}}
, @@attribute.type@@ @@attribute.name-@@
{{@- if attribute.array_attributes is defined -@}}
{{@- for aattribute in attribute.array_attributes: -@}}
, const @@aattribute.type@@* @@aattribute.name@@
{{@- endfor -@}}
{{@- endif -@}}
{{@- endfor @}} ){
	TimeSlicer *timeSlicer = (TimeSlicer *)callbackHolder;
{{@ if definition.name in ["MpiSend","MpiIsend","MpiIsendComplete","MpiIrecvRequest","MpiRecv","MpiIrecv","MpiCollectiveBegin","MpiCollectiveEnd","RmaCollectiveBegin","RmaCollectiveEnd","ThreadTeamBegin","ThreadTeamEnd","ThreadCreate","ThreadBegin","ThreadWait","ThreadEnd","IoOperationBegin","IoOperationComplete","IoOperationCancelled","ProgramBegin","ProgramEnd"] @}}
	OTF2_TimeStamp changedTime = @@timeparam@@;
	if(@@timeparam@@ < timeSlicer->lowerBound){
		changedTime = timeSlicer->lowerBound;
		//printf("timestamp < lower bound(%"PRIu64"):%"PRIu64"->%"PRIu64"\n", timeSlicer->lowerBound, @@timeparam@@, changedTime);
{{@ if definition.name in ["ThreadCreate","ThreadWait"] and definitiontype == "Evt": @}}
		mapentry *entry = map_find(&(timeSlicer->locationStates),AS_VOID_PTR(location));
		LocationState *state = (LocationState *) entry->to;
		OTF2_AttributeList* emptyAttributeList = OTF2_AttributeList_New();
		callbackHolder->next->on@@definitiontype@@_Enter(location, changedTime, 0, callbackHolder->next, emptyAttributeList, state->lastEnteredRegion);
		state->writingLeaveRequired = true;
		OTF2_AttributeList_Delete(emptyAttributeList);
{{@ endif @}}
	}
	if(@@timeparam@@ > timeSlicer->upperBound){
		changedTime = timeSlicer->upperBound;
{{@ if definition.name in ["ThreadCreate","ThreadWait"] and definitiontype == "Evt": @}}
		mapentry *entry = map_find(&(timeSlicer->locationStates),AS_VOID_PTR(location));
		LocationState *state = (LocationState *) entry->to;
		OTF2_AttributeList* emptyAttributeList = OTF2_AttributeList_New();
		callbackHolder->next->on@@definitiontype@@_Enter(location, changedTime, 0, callbackHolder->next, emptyAttributeList, state->lastEnteredRegion);
		list_add(&(state->regionStackInSlice), AS_VOID_PTR(state->lastEnteredRegion));
		OTF2_AttributeList_Delete(emptyAttributeList);
{{@ endif @}}
		//printf("timestamp > upper bound(%"PRIu64"):%"PRIu64"->%"PRIu64"\n", timeSlicer->upperBound, @@timeparam@@, changedTime);
	}
	return callbackHolder->next->on@@definitiontype@@_@@definition.name@@(location,changedTime@@addparam@@,callbackHolder->next,attributeList
{{@- for attribute in definition.attributes: -@}}
,@@attribute.name-@@
{{@- if attribute.array_attributes is defined -@}}
{{@- for aattribute in attribute.array_attributes: -@}}
,@@aattribute.name@@
{{@- endfor -@}}
{{@- endif -@}}
{{@- endfor @}} );
{{@ elif definition.name == "Enter" and definitiontype == "Evt": @}}
	OTF2_TimeStamp changedTime = @@timeparam@@;
	mapentry *entry = map_find(&(timeSlicer->locationStates),AS_VOID_PTR(location));
	LocationState *state = (LocationState *) entry->to;
	state->lastEnteredRegion = region;
	state->writingLeaveRequired = false;
	if(@@timeparam@@ < timeSlicer->lowerBound){
		// We need to write this enter event only if the corresponding leave event is behind lower bound of time...
		list_add(&(state->regionStackBeforeSlice), AS_VOID_PTR(region));
		return OTF2_CALLBACK_SUCCESS;
	}
	OTF2_AttributeList* emptyAttributeList = OTF2_AttributeList_New();
	// We have collected some enter events before and the current enter event is in or after the time interval, so write previous enter events.
	for(int i = 0; i < state->regionStackBeforeSlice.entryCount; i++){
		OTF2_RegionRef enteredRegion = AS_REGION_REF(state->regionStackBeforeSlice.entries[i]);
		callbackHolder->next->on@@definitiontype@@_Enter(location, timeSlicer->lowerBound, 0, callbackHolder->next, emptyAttributeList, enteredRegion);
		list_add(&(state->regionStackInSlice), AS_VOID_PTR(enteredRegion));
	}
	OTF2_AttributeList_Delete(emptyAttributeList);
	state->regionStackBeforeSlice.entryCount = 0;
	if(@@timeparam@@ > timeSlicer->lowerBound && @@timeparam@@ < timeSlicer->upperBound){
		// Current enter event is in the interval -> remember it for leave events after the interval + write it.
		list_add(&(state->regionStackInSlice), AS_VOID_PTR(region));
		return callbackHolder->next->on@@definitiontype@@_@@definition.name@@(location,changedTime@@addparam@@,callbackHolder->next,attributeList
{{@- for attribute in definition.attributes: -@}}
,@@attribute.name-@@
{{@- if attribute.array_attributes is defined -@}}
{{@- for aattribute in attribute.array_attributes: -@}}
,@@aattribute.name@@
{{@- endfor -@}}
{{@- endif -@}}
{{@- endfor @}} );
	}
	return OTF2_CALLBACK_SUCCESS;
{{@ elif definition.name == "Leave": @}}
	OTF2_TimeStamp changedTime = @@timeparam@@;
	mapentry *entry = map_find(&(timeSlicer->locationStates),AS_VOID_PTR(location));
	LocationState *state = (LocationState *) entry->to;
	if(@@timeparam@@ < timeSlicer->lowerBound){
		// We are before the time interval and this event seems important, so we write it, but with new timestamp.
		state->regionStackBeforeSlice.entryCount = MAX_VALUE(0, state->regionStackBeforeSlice.entryCount - 1);
		if(state->writingLeaveRequired){
			state->writingLeaveRequired = false;
			return callbackHolder->next->on@@definitiontype@@_@@definition.name@@(location,timeSlicer->lowerBound@@addparam@@,callbackHolder->next,attributeList
{{@- for attribute in definition.attributes: -@}}
,@@attribute.name-@@
{{@- if attribute.array_attributes is defined -@}}
{{@- for aattribute in attribute.array_attributes: -@}}
,@@aattribute.name@@
{{@- endfor -@}}
{{@- endif -@}}
{{@- endfor @}} );
		}
		return OTF2_CALLBACK_SUCCESS;
	}
	// We might have collected some enter events before and the current leave event is in or after the time interval, so write previous enter event.
	if(state->writingLeaveRequired){
		if(state->regionStackBeforeSlice.entryCount > 0){
			changedTime = timeSlicer->lowerBound;
		}
		state->regionStackBeforeSlice.entryCount = MAX_VALUE(0, state->regionStackBeforeSlice.entryCount - 1);
		callbackHolder->next->on@@definitiontype@@_@@definition.name@@(location,changedTime@@addparam@@,callbackHolder->next,attributeList
{{@- for attribute in definition.attributes: -@}}
,@@attribute.name-@@
{{@- if attribute.array_attributes is defined -@}}
{{@- for aattribute in attribute.array_attributes: -@}}
,@@aattribute.name@@
{{@- endfor -@}}
{{@- endif -@}}
{{@- endfor @}} );
		state->writingLeaveRequired = false;
	}
	OTF2_AttributeList* emptyAttributeList = OTF2_AttributeList_New();
	// We have collected some enter events before the interval and the current leave event is in or after the time interval, so write enter events from before the time interval that were not matched by a leave event so far.
	for(int i = 0; i < state->regionStackBeforeSlice.entryCount; i++){
		OTF2_RegionRef enteredRegion = AS_REGION_REF(state->regionStackBeforeSlice.entries[i]);
		callbackHolder->next->on@@definitiontype@@_Enter(location, timeSlicer->lowerBound, 0, callbackHolder->next, emptyAttributeList, enteredRegion);
		list_add(&(state->regionStackInSlice), AS_VOID_PTR(enteredRegion));
	}
	state->regionStackBeforeSlice.entryCount = 0;
	// We have collected some events in the time interval and the current leave event is in the time interval, so remove a single enter event from the time interval and write corresponding leave.
	if(@@timeparam@@ > timeSlicer->lowerBound && @@timeparam@@ < timeSlicer->upperBound){
		state->regionStackInSlice.entryCount = MAX_VALUE(0, state->regionStackInSlice.entryCount - 1);
		OTF2_AttributeList_Delete(emptyAttributeList);
		return callbackHolder->next->on@@definitiontype@@_@@definition.name@@(location,changedTime@@addparam@@,callbackHolder->next,attributeList
{{@- for attribute in definition.attributes: -@}}
,@@attribute.name-@@
{{@- if attribute.array_attributes is defined -@}}
{{@- for aattribute in attribute.array_attributes: -@}}
,@@aattribute.name@@
{{@- endfor -@}}
{{@- endif -@}}
{{@- endfor @}} );
	}
	// We have collected some events in the time interval and the current leave event is after the time interval, so write corresponding leave events with modified timestamp for all remaining enter events.
	for(int i = state->regionStackInSlice.entryCount - 1; i >= 0; i--){
		OTF2_RegionRef enteredRegion = AS_REGION_REF(state->regionStackInSlice.entries[i]);
		callbackHolder->next->on@@definitiontype@@_Leave(location, timeSlicer->upperBound, 0, callbackHolder->next, emptyAttributeList, enteredRegion);
	}
	state->regionStackInSlice.entryCount = 0;
	OTF2_AttributeList_Delete(emptyAttributeList);
	return OTF2_CALLBACK_SUCCESS;
{{@ else @}}
	if((timeSlicer->lowerBound < @@timeparam@@) && (@@timeparam@@ < timeSlicer->upperBound)){
		return callbackHolder->next->on@@definitiontype@@_@@definition.name@@(location,@@timeparam@@@@addparam@@,callbackHolder->next,attributeList
{{@- for attribute in definition.attributes: -@}}
,@@attribute.name-@@
{{@- if attribute.array_attributes is defined -@}}
{{@- for aattribute in attribute.array_attributes: -@}}
,@@aattribute.name@@
{{@- endfor -@}}
{{@- endif -@}}
{{@- endfor @}} );
	}
	return OTF2_CALLBACK_SUCCESS;
{{@ endif @}}
}
{{@ endfor -@}}
{{@ endfor @}}

static OTF2_CallbackCode calculateBoundsFromClockProperties_cb( CallbackHolder *callbackHolder, uint64_t timerResolution, uint64_t globalOffset, uint64_t traceLength, uint64_t realtimeTimestamp ){
	TimeSlicer *timeSlicer = (TimeSlicer *) callbackHolder;
	timeSlicer->lowerBound = MAX_VALUE(globalOffset,timeSlicer->lowerBoundInSeconds*timerResolution+globalOffset);
	uint64_t diffFromGlobalOffset = timeSlicer->upperBoundInSeconds*timerResolution;
	if( traceLength != 0){
		diffFromGlobalOffset = MIN_VALUE(traceLength,diffFromGlobalOffset);
	}
	timeSlicer->upperBound = globalOffset+diffFromGlobalOffset;
	//printf("clock: [%"PRIu64",%"PRIu64"]\n", timeSlicer->lowerBound, timeSlicer->upperBound);
	return callbackHolder->next->onGlobalDef_ClockProperties( callbackHolder->next, timerResolution, timeSlicer->lowerBound, timeSlicer->upperBound-timeSlicer->lowerBound, realtimeTimestamp );
}

static LocationState *createLocationState(void){
	LocationState *locationState = (LocationState *)malloc(sizeof(LocationState));
	if(NULL != locationState){
		list_initialize(&(locationState->regionStackBeforeSlice));
		list_initialize(&(locationState->regionStackInSlice));
	}
	return locationState;
}

static OTF2_CallbackCode TimeSlicer_GlobalDef_Location_cb( CallbackHolder *callbackHolder, OTF2_LocationRef self, OTF2_StringRef name, OTF2_LocationType locationType, uint64_t numberOfEvents, OTF2_LocationGroupRef locationGroup ){
	TimeSlicer *timeSlicer = (TimeSlicer *) callbackHolder;
	LocationState *locationState = createLocationState();
	map_add(&(timeSlicer->locationStates), AS_VOID_PTR(self), locationState);
	return callbackHolder->next->onGlobalDef_Location( callbackHolder->next, self, name, locationType, numberOfEvents, locationGroup );
}

void transformIntoDefaultCallbackHolder(CallbackHolder *callbackHolder);

static void del_locationState(void *from, void *to){
	LocationState *state = (LocationState *)to;
	list_delete(&(state->regionStackBeforeSlice), del_list_element_func_nothing);
	list_delete(&(state->regionStackInSlice), del_list_element_func_nothing);
	free(state);
}

static void releaseTimeSlicer(TimeSlicer *timeSlicer){
	map_delete(&(timeSlicer->locationStates), del_locationState);
	free(timeSlicer);
}

Pipeline *createTimeSlicer(double fromInSeconds, double toInSeconds){
	TimeSlicer *timeSlicer = (TimeSlicer *)malloc(sizeof(TimeSlicer));
	if(NULL != timeSlicer){
		transformIntoDefaultCallbackHolder(&(timeSlicer->callbackHolder));
		timeSlicer->callbackHolder.onGlobalDef_ClockProperties = calculateBoundsFromClockProperties_cb;
		timeSlicer->callbackHolder.onGlobalDef_Location = TimeSlicer_GlobalDef_Location_cb;
{{@ for definitiontype,fil in [("Evt",events),("Snap",snaps)] @}}
		timeSlicer->callbackHolder.on@@definitiontype@@_Unknown = &@@definitiontype@@_Unknown_TimeSlice_cb;
{{@- for definition in fil: @}}
		timeSlicer->callbackHolder.on@@definitiontype@@_@@definition.name@@ = &@@definitiontype@@_@@definition.name@@_TimeSlice_cb;
{{@- endfor @}}
{{@- endfor @}}
		timeSlicer->lowerBoundInSeconds = fromInSeconds;
		timeSlicer->upperBoundInSeconds = toInSeconds;
		map_initialize(&(timeSlicer->locationStates),compareMapInt,hashUint32);
	}
	return asPipeline((CallbackHolder *)timeSlicer, (ReleaseCallback)releaseTimeSlicer);
}

