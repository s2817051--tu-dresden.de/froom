/* Author: Jan Frenzel */
{{@- set definitionList = ["string","location","region","communicator","group","systemTreeNode","locationGroup","paradigm","attribute","callpath","parameter","metricMember","metric","metricInstance","ioParadigm","ioFile","ioHandle","cartDimension", "cartTopology", "rmaWin"] @}}
#include <unistd.h>
#include <stdlib.h>
#include "utils.h"
#include "map.h"

typedef struct _translator_t translator_t;

struct _translator_t{
{{@- for x in definitionList @}}
	map @@x@@; /* OTF2_@@x[0].upper()@@@@x[1:]@@Ref -> OTF2_@@x[0].upper()@@@@x[1:]@@Ref */
{{@- endfor @}}
};

void translator_initialize(translator_t *translator){
{{@- for x in definitionList @}}
	map_initialize(&(translator->@@x@@), compare_key, hashUint32);
{{@- endfor @}}
}

translator_t *translator_Create(void){
	translator_t *translator = (translator_t *)malloc(sizeof(translator_t));
	translator_initialize(translator);
	return translator;
}

void translator_finalize(translator_t *translator){
{{@- for x in definitionList @}}
	map_delete(&(translator->@@x@@),del_func_nothing);
{{@- endfor @}}
}

void translator_Delete(translator_t *translator){
	translator_finalize(translator);
	free(translator);
}

typedef OTF2_Paradigm OTF2_ParadigmRef;
{{@ for x in definitionList @}}
{{@- set a = "Comm" if x=="communicator" else x[0].upper()+x[1:] @}}
{{@- if a != "MetricInstance" -@}}
OTF2_@@a@@Ref translate_or_default_@@a@@(map *@@a@@TranslationMap, OTF2_@@a@@Ref ref){
	mapentry *entry = NULL;
	OTF2_@@a@@Ref to_return;
	if((entry = map_find(@@a@@TranslationMap,AS_VOID_PTR(ref))) != NULL ){
		to_return=(OTF2_@@a@@Ref)(uintptr_t)(entry->to);
	}else{
		to_return=ref;
	}
	return to_return;
}
{{@- endif @}}
{{@ endfor @}}
