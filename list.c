/* Author: Jan Frenzel */
#include <stdlib.h>
#include <stddef.h>
#include "list.h"
#define DEFAULT_CAPACITY 64

void list_initialize(list *theList){
	theList->entryCount = 0;
	theList->capacity = DEFAULT_CAPACITY;
	theList->entries = malloc(theList->capacity * sizeof(void *));
}

void list_add( list *theList, const void *const element ){
	if(theList->capacity == 0) {
		list_initialize(theList);
	}else if(theList->entryCount == theList->capacity) {
		void *newEntries = NULL;
		theList->capacity = theList->capacity * 2;
		newEntries = realloc(theList->entries, theList->capacity * sizeof(void *));
		if(newEntries == NULL){
			abort();
		}else{
			theList->entries = newEntries;
		}
	}
	theList->entries[theList->entryCount] = element;
	theList->entryCount ++;
}

void list_add_if_not_contained(list *theList, void *element, int (*compare_func)(const void *, const void *)){
	qsort(theList->entries, theList->entryCount, sizeof(void *), compare_func);
	if(NULL == bsearch(&element, theList->entries, theList->entryCount, sizeof(void *), compare_func)){
		list_add(theList, element);
	}
}

uint32_t list_index(list *theList, void *element, int (*compare_func)(const void *, const void *)){
	void *found = bsearch(&element, theList->entries, theList->entryCount, sizeof(void *), compare_func);
	if(NULL == found){
		return UINT32_MAX;
	}
	void **realVal = (void **) found;
	ptrdiff_t diff = realVal - theList->entries;
	return (uint32_t) diff;
}

bool list_contains(list *theList, const void *const element){
	int entryCount = (theList->capacity == 0)?0:theList->entryCount;
	for(int i = 0; i < entryCount; i++){
		void *entry = theList->entries[i];
		if(entry == element){
			return true;
		}
	}
	return false;
}

void list_delete(list /*by ref*/ *theList, list_element_data_func del_func){
	int entryCount = (theList->capacity == 0)?0:theList->entryCount;
	for(int i = 0; i < entryCount; i++){
		void *entry = theList->entries[i];
		del_func(entry);
	}
	theList->entryCount = 0;
	if(theList->capacity != 0){
		free(theList->entries);
	}
	theList->capacity = 0;
}

void del_list_element_func_nothing(void *element){
}

void del_list_element_func_free(void *element){
	free(element);
}

int compareInt(const void *firstPtr, const void *secondPtr){
	void *first = *(void **)firstPtr;
	void *second = *(void **)secondPtr;
	int firstVal = (int)(intptr_t)first;
	int secondVal = (int)(intptr_t)second;
	return firstVal - secondVal;
}
