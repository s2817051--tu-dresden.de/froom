/* Author: Jan Frenzel */
#include <stdbool.h>
#include <inttypes.h>
#ifndef _LIST_H_INCLUDED
#define _LIST_H_INCLUDED
typedef struct _list list;
struct _list{
	void **entries;
	int entryCount;
	int capacity;
};

void list_initialize(list *theList);

void list_add(list *theList, const void *const element);

void list_add_if_not_contained(list *theList, void *element, int (*compare_func)(const void *, const void *));

uint32_t list_index(list *theList, void *element, int (*compare_func)(const void *, const void *));

bool list_contains(list *theList, const void *const element);

typedef void (*list_element_data_func)(void *element);

void list_delete(list /*by ref*/ *theList, list_element_data_func del_func);

void del_list_element_func_nothing(void *element);

void del_list_element_func_free(void *element);

int compareInt(const void *firstPtr, const void *secondPtr);
#endif /*_LIST_H_INCLUDED */
