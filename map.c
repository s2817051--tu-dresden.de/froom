/* Author: Jan Frenzel */
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include "map.h"
#include <string.h>

#define DEFAULT_CAPACITY 128

void map_initialize(map *mapping, compare_func sort_func, hash_func key_hash_func){
	for(int i = 0; i < HASHTABLE_ENTRY_COUNT; i++){
		mapbucket *bucket = &(mapping->buckets[i]);
		bucket->entryCount = 0;
		bucket->capacity = DEFAULT_CAPACITY;
		bucket->entries = calloc(bucket->capacity, sizeof(mapentry));
	}
	mapping->sort_func = sort_func;
	mapping->key_hash_func = key_hash_func;
}

void map_add( map *mapping, const void *const from, const void *const to ){
	uint64_t hash = mapping->key_hash_func(from) % HASHTABLE_ENTRY_COUNT;
	mapbucket *bucket = &(mapping->buckets[hash]);
	if(bucket->entryCount == bucket->capacity) {
		mapentry *newEntries = NULL;
		bucket->capacity = bucket->capacity * 2;
		newEntries = realloc(bucket->entries, bucket->capacity * sizeof(mapentry));
		if(newEntries == NULL){
			abort();
		}else{
			bucket->entries = newEntries;
		}
	}
	mapentry *entry = &(bucket->entries[bucket->entryCount]);
	entry->from = (void *) from;
	entry->to = (void *) to;
	bucket->entryCount ++;
	if(bucket->entryCount > 1){
		mapentry *previousEntry = &(bucket->entries[bucket->entryCount - 2]);
		if((mapping->sort_func)(previousEntry, entry) >= 0){
			qsort(bucket->entries, bucket->entryCount, sizeof(mapentry), (mapping->sort_func));
		}
	}
}

mapentry *map_find_first(map *const mapping, const void *const from){
	if(mapping == NULL) {
		return NULL;
	}
	const mapentry toBeFound = { from, NULL };
	uint64_t hash = mapping->key_hash_func(from) % HASHTABLE_ENTRY_COUNT;
	mapbucket *bucket = &(mapping->buckets[hash]);
	mapentry *result = bsearch(&toBeFound, bucket->entries, bucket->entryCount, sizeof(mapentry), mapping->sort_func);
	if(result != NULL){
		// entries should be sorted
		while(result >= bucket->entries && 0 == (mapping->sort_func)(result, &toBeFound)){
			result--;
		}
		result++;
	}
	return result;
}

mapentry *map_find_next(map *const mapping, const void *const from, mapentry *const currentResult){
	if(mapping == NULL || currentResult == NULL) {
		return NULL;
	}
	mapentry *entry = currentResult + 1;
	uint64_t hash = mapping->key_hash_func(from) % HASHTABLE_ENTRY_COUNT;
	mapbucket *bucket = &(mapping->buckets[hash]);
	if(entry < (bucket->entries + bucket->entryCount) && (mapping->sort_func)(entry, currentResult) == 0){
		return entry;
	}
	return NULL;
}

mapentry *map_find(map *const mapping, const void *const from){
	if(mapping == NULL) {
		return NULL;
	}
	const mapentry toBeFound = { from, NULL };
	uint64_t hash = mapping->key_hash_func(from) % HASHTABLE_ENTRY_COUNT;
	mapbucket *bucket = &(mapping->buckets[hash]);
	return bsearch(&toBeFound, bucket->entries, bucket->entryCount, sizeof(mapentry), (mapping->sort_func));
}

void *map_get(map *const mapping, const void *const from){
	mapentry *result;
	result = map_find(mapping, from);
	if(NULL == result){
		return NULL;
	}
	return result->to;
}

void map_delete(map /*by ref*/ *mapping, element_data_func del_func){
	for(int i = 0; i < HASHTABLE_ENTRY_COUNT; i++){
		mapbucket *bucket = &(mapping->buckets[i]);
		int entryCount = (bucket->capacity == 0)?0:bucket->entryCount;
		for(int i = 0; i < entryCount; i++){
			mapentry *entry = &(bucket->entries[i]);
			del_func(entry->from, entry->to);
		}
		bucket->entryCount = 0;
		if(bucket->capacity != 0){
			free(bucket->entries);
		}
		bucket->capacity = 0;
	}
}

void map_iterate(map /*by ref*/ *mapping, element_data_func_with_additional_data item_func, void *additionalData){
	for(int i = 0; i < HASHTABLE_ENTRY_COUNT; i++){
		mapbucket *bucket = &(mapping->buckets[i]);
		int entryCount = (bucket->capacity == 0)?0:bucket->entryCount;
		for(int i = 0; i < entryCount; i++){
			mapentry *entry = &(bucket->entries[i]);
			item_func(entry->from, entry->to, additionalData);
		}
	}
}

int map_entry_count(map *mapping){
	int entryCountTotal = 0;
	for(int i = 0; i < HASHTABLE_ENTRY_COUNT; i++){
		mapbucket *bucket = &(mapping->buckets[i]);
		int entryCount = (bucket->capacity == 0)?0:bucket->entryCount;
		entryCountTotal += entryCount;
	}
	return entryCountTotal;
}

void del_func_nothing(void *from, void *to){
}

void del_func_free_from(void *from, void *to){
	free(from);
}

void del_func_free_all(void *from, void *to){
	free(from);
	free(to);
}

int compare_key(const void *firstPtr, const void *secondPtr){
	const mapentry *firstEntry = (const mapentry *) firstPtr;
	const mapentry *secondEntry = (const mapentry *) secondPtr;
	return (firstEntry->from > secondEntry->from) - (firstEntry->from < secondEntry->from);
}

int compareMapInt(const void *first, const void *second){
	mapentry *firstEntry = (mapentry *)first;
	mapentry *secondEntry = (mapentry *)second;
	int firstVal = (int)(intptr_t)(firstEntry->from);
	int secondVal = (int)(intptr_t)(secondEntry->from);
	return firstVal - secondVal;
}

int compareStringMapFrom(const void *firstPtr, const void *secondPtr){
	const mapentry *firstEntry = (const mapentry *) firstPtr;
	const mapentry *secondEntry = (const mapentry *) secondPtr;
	return strcmp((const char *)(firstEntry->from), (const char *)(secondEntry->from));
}

int compareColumnIndexesFromMap(const void *firstPtr, const void *secondPtr){
	mapentry *firstEntry = (mapentry *)firstPtr;
	mapentry *secondEntry = (mapentry *)secondPtr;
	uint64_t firstVal = (uint64_t)(intptr_t)(firstEntry->from);
	uint64_t secondVal = (uint64_t)(intptr_t)(secondEntry->from);
	return (firstVal > secondVal) - (firstVal < secondVal);
}

int unsorted_sort_func(const void *first, const void *second){
        return -1;
}

uint64_t hashString(const void *stringKey){
	uint64_t hash = 0;
	char *str = (char *)stringKey;
	for(; '\0' != *str; str++){
		hash = 31*hash + *str;
	}
	return hash;
}

uint64_t hashUint32(const void *uint32Key){
	return (uint32_t)(uintptr_t)uint32Key;
}

uint64_t hashToConstant(const void *key){
	return 0;
}
