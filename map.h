/* Author: Jan Frenzel */
#include <stdbool.h>
#include <stdlib.h>
#ifndef _MAP_H_INCLUDED
#define _MAP_H_INCLUDED
typedef struct _mapentry mapentry;
struct _mapentry{
	void *from;
	void *to;
};

typedef int (*compare_func)(const void *,const void *);
typedef uint64_t (*hash_func)(const void *);

#define HASHTABLE_ENTRY_COUNT 127
typedef struct _mapbucket mapbucket;
struct _mapbucket{
	mapentry *entries;
	int entryCount;
	int capacity;
};

typedef struct _map map;
struct _map{
	mapbucket buckets[HASHTABLE_ENTRY_COUNT];
	compare_func sort_func;
	hash_func key_hash_func;
};

void map_initialize(map *mapping, compare_func sort_func, hash_func key_hash_func);

void map_add(map *mapping, const void *const from, const void *const to);

typedef void (*element_data_func)(void *from, void *to);
typedef void (*element_data_func_with_additional_data)(void *from, void *to, void *additionalData);

mapentry *map_find_first(map *const mapping, const void *const from);

mapentry *map_find_next(map *const mapping, const void *const from, mapentry *const currentResult);

mapentry *map_find(map *const mapping, const void *const from);

void *map_get(map *const mapping, const void *const from);

void map_delete(map /*by ref*/ *mapping, element_data_func del_func);

void map_iterate(map /*by ref*/ *mapping, element_data_func_with_additional_data item_func, void *additionalData);

int map_entry_count(map *mapping);

void del_func_nothing(void *from, void *to);

void del_func_free_from(void *from, void *to);

void del_func_free_all(void *from, void *to);

int compare_key(const void *firstPtr, const void *secondPtr);

int compareMapInt(const void *first, const void *second);

int compareStringMapFrom(const void *firstPtr, const void *secondPtr);

int compareColumnIndexesFromMap(const void *firstPtr, const void *secondPtr);
/**
 * if map_find will never be used:
 */
int unsorted_sort_func(const void *firstPtr, const void *secondPtr);

uint64_t hashString(const void *stringKey);

uint64_t hashUint32(const void *uint32Key);

/**
 * if map_find will never be used:
 */
uint64_t hashToConstant(const void *uint32Key);
#endif /*_MAP_H_INCLUDED */
