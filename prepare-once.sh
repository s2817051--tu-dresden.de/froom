#!/bin/bash
if [ $# -ne 1 ]
then
cat <<-EOF
Usage: $0 <otf2-base-dir>
EOF
exit 1
fi
OTF2_BASE=$1
cat <<EOF > generator/otf2paths
OTF2_BASE=$OTF2_BASE
EOF
