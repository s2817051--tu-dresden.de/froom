#include "stable_marriage.h"
typedef int (*PREFERENCE_COMPARE)(void *person, void *oppositePerson1, void *oppositePerson2);

static PREFERENCE_COMPARE personPreferenceCompareFn;
static void *relevantPerson;
static void **relevantOppositePersons;

static int comparePersonPreferenceQsort(const void *person1IdxPtr, const void *person2IdxPtr){
	const int person1Idx = *(const int *)person1IdxPtr;
	const int person2Idx = *(const int *)person2IdxPtr;
	return personPreferenceCompareFn(relevantPerson, relevantOppositePersons[person1Idx], relevantOppositePersons[person2Idx]);
}

static int **createPreferenceTable(list *persons, list *oppositePersons){
	int personCount = persons->entryCount;
	int oppositePersonCount = oppositePersons->entryCount;
	int **personPreferenceTable = (int **)calloc(personCount, sizeof(int *));
	int *personsPreferenceList = (int *)calloc(personCount, oppositePersonCount * sizeof(int));
	for(int i = 0; i < personCount; i++){
		personPreferenceTable[i] = personsPreferenceList;
		for(int j = 0; j < oppositePersonCount; j++){
			personsPreferenceList[j] = j;
		}
		relevantPerson = persons->entries[i];
		relevantOppositePersons = oppositePersons->entries;
		qsort(personsPreferenceList, personCount, sizeof(int), comparePersonPreferenceQsort);
		personsPreferenceList += oppositePersonCount;
	}
	return personPreferenceTable;
}

#define NOT_ENGAGED -1

/*
 * initialize every man m and woman w to free
 * while a free man m exists who has a woman to propose to do
 *   take first woman w to whom m has not proposed
 *   if w has a fiance m' then
 *     if w prefers m over m' then
 *       m' becomes free
 *       m and w become engaged
 *     end if
 *   else
 *     m and w become engaged
 *   end if
 * end while
 */

void stableMarriage(map *mapToPutEngagements, list *men, list *women, PREFERENCE_COMPARE preference_compare){
	int menCount = (men->entryCount <= women->entryCount)? men->entryCount : women->entryCount;
	int womenCount = women->entryCount;
	void **womenEntries = women->entries;
	void **menEntries = men->entries;
	personPreferenceCompareFn = preference_compare;
	int **mensPreferenceTable = createPreferenceTable(men, women);
	// initialize every man m and woman w to free
	int *mensPropsals = (int *)calloc(2*menCount+womenCount, sizeof(int));
	int *mensEngagement = mensPropsals+menCount;
	int *womensEngagement = mensEngagement+menCount;
	for(int i = 0; i < 2*menCount+womenCount; i++){
		mensPropsals[i] = -1; // NOT_ENGAGED is also -1
	}
	int someMan = 0;
	do{
		// a free man m exists
		int theMansProposal = ++(mensPropsals[someMan]);
		if(theMansProposal < womenCount){
			int *theMansPreferredWomen = mensPreferenceTable[someMan];
 			//  ... who has a woman to propose to
 			//  take first woman w to whom m has not proposed
			int woman = theMansPreferredWomen[theMansProposal];
			int fiance = womensEngagement[woman];
			if(fiance != NOT_ENGAGED){
				// w has a fiance m'
				if(preference_compare(womenEntries[woman], menEntries[someMan], menEntries[fiance]) <= 0){
					// w prefers m over m'
					// m' becomes free
					// m and w become engaged
					mensEngagement[fiance] = NOT_ENGAGED;
					mensEngagement[someMan] = woman;
					womensEngagement[woman] = someMan;
				}
			}else{
				// m and w become engaged
				mensEngagement[someMan] = woman;
				womensEngagement[woman] = someMan;
			}
		}
		someMan = 0;
		for(; someMan < menCount && mensEngagement[someMan] != NOT_ENGAGED; someMan++){
		}
	}while(someMan < menCount);
	for(int i = 0; i < menCount; i++){
		map_add(mapToPutEngagements, menEntries[i], womenEntries[mensEngagement[i]]);
	}
	free(mensPreferenceTable[0]);
	free(mensPreferenceTable);
	free(mensPropsals);
}
