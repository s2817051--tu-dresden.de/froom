#include "list.h"
#include "map.h"
typedef int (*PREFERENCE_COMPARE)(void *person, void *oppositePerson1, void *oppositePerson2);
void stableMarriage(map *mapToPutEngagements, list *men, list *women, PREFERENCE_COMPARE preference_compare);
