/* Author: Jan Frenzel */
#include <stdio.h>
#define MAX_VALUE(x,y) ((x)>(y))?(x):(y)
#define MIN_VALUE(x,y) ((x)<(y))?(x):(y)
#ifndef AS_VOID_PTR_DEFINED
#define AS_VOID_PTR(a) (void*)(intptr_t)(a)
#endif
#define AS_GROUP_REF(a) (OTF2_GroupRef)(uintptr_t)(a)
#define AS_CARTTOPOLOGY_REF(a) (OTF2_CartTopologyRef)(uintptr_t)(a)
#define AS_RMAWIN_REF(a) (OTF2_RmaWinRef)(uintptr_t)(a)
#define AS_COMM_REF(a) (OTF2_CommRef)(uintptr_t)(a)
#define AS_REGION_REF(a) (OTF2_RegionRef)(uintptr_t)(a)
#define AS_METRIC_REF(a) (OTF2_MetricRef)(uintptr_t)(a)
#define AS_LOCATION_REF(a) (OTF2_LocationRef)(uintptr_t)(a)
#define AS_STRING_REF(a) (OTF2_StringRef)(uintptr_t)(a)
#define COMPARE_TYPE(func) (compare_func)compare##func##MapFrom
#ifdef DEBUG
#define debug_log(x) fputs("writing: " x "\n", stderr)
#else
#define debug_log(x)
#endif
